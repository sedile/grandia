#include <iostream>

#include "Bag.h"
#include "StandardItem.h"
#include "GearItem.h"

using namespace grandia;

/**
 * @brief Bag::Bag Default Constructor
 */
Bag::Bag() { }

/**
 * @brief Bag::Bag Constructor which initialises the itembag
 * @param bag vector of items
 */
Bag::Bag(std::vector<Item*> &bag)
: _itemindex(0) { 
    if ( bag.size() <= 10 ) {
        _items = bag;
    }
}

/**
 * @brief Bag::~Bag Destructor. Deletes the itemvector
 */
Bag::~Bag() { 
    for(num i = 0; i < _items.size(); i++){
        delete _items.at(i);
    }
}

/**
 * @brief Bag::addItem add an item in the bag
 * @param item item to add
 */
void Bag::addItem(Item* item){
    _items.push_back(item);
}

/**
 * @brief Bag::printItemList prints a list off all actors items
 */
void Bag::printItemList() {
    std::cout << "IDX " << _itemindex << "\n";
    for(num i = 0; i < _items.size(); i++){
        if ( _items.at(i)->isStandardItem() ){
            StandardItem *si = dynamic_cast<StandardItem*>(_items.at(i));
            std::cout << "[" << std::to_string(i) << "] ";
            std::cout << si->getName() << " | ";
            std::cout << si->getDescription() << " | ";
            std::cout << std::to_string(si->getValue()) << " | ";
            std::cout << si->getType() << " | ";
            std::cout << std::to_string(si->getStatusFlag()) << "\n";
        } else {
            GearItem *gi = dynamic_cast<GearItem*>(_items.at(i));
            std::cout << "[x] ";
            std::cout << gi->getName() << " | ";
            std::cout << " < is a gear item >" << "\n";
        }
    }
}

/**
 * @brief Bag::getSize returns the size of this itembag
 * @return size
 */
num Bag::getSize() const {
    return _items.size();
}

/**
 * @brief Bag::deleteItem deletes an item from the bag
 */
void Bag::deleteItem() {
    _items.erase(_items.begin() + _itemindex); // TODO : itemindex hat Wert 0
    //_itemindex = 0;
}

/**
 * @brief Bag::getItem gets an item from the bag
 * @param idx index of the vector
 * @return item
 */
Item* Bag::getItem(const num &idx){
    if ( idx > _items.size() ){
        return nullptr;
    }
    _itemindex = idx;
    return _items.at(idx);
}
