#ifndef __BAG_H__
#define __BAG_H__

#include "Item.h"

namespace grandia {

    class Bag {
    private:
        std::vector<Item*> _items;
        num _itemindex;
    public:
        explicit Bag();
        explicit Bag(std::vector<Item*> &bag);
        virtual ~Bag();

        void addItem(Item* item);
        void printItemList();
        void deleteItem();
        num getSize() const;
        Item* getItem(const num &idx);
    };

}

#endif
