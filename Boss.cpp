#include "Boss.h"

using namespace grandia;

Boss::Boss(std::string name, bool isMainpart, num mhp, num mmp, num msp, num level, num maxXP, num money, num str, num def, num spd, num mov)
    : Enemy(name,mhp,mmp,msp,level,maxXP,money,str,def,spd,mov), IS_MAINPART(isMainpart) { }

Boss::~Boss(){ }

bool Boss::isMainpart() const {
    return IS_MAINPART;
}
