#ifndef BOSS_H
#define BOSS_H

#include "Enemy.h"

namespace grandia {

class Boss : public Enemy{
private:
    const bool IS_MAINPART;
public:
    explicit Boss(std::string name, bool isMainpart, num mhp, num mmp, num msp, num level, num maxXP, num money, num str, num def, num spd, num mov);
    virtual ~Boss();

    bool isMainpart() const;
};

}

#endif // BOSS_H
