#ifndef __CODE_H__
#define __CODE_H__

namespace grandia {

    typedef unsigned short num;

    struct MenuCode {
        /* Menuwheel */
        static const char DEFEND = 'd';
        static const char EVADE = 'e';
        static const char MAGIC = 'm';
        static const char SPECIAL = 's';
        static const char COMBO = 'c';
        static const char CRITICAL = 'x';
        static const char ITEM = 'u';
        static const char INFO = 'i';
    };

    struct AttackCode {
        /* Attacktypes */
        static const num ATTACK = 101;
        static const num HEAL = 102;
        static const num BOOST = 103;
        static const num DECREASE = 104;
    };

    struct StatusCode {
        /* Types */
        static const num NO_STATUS = 201;
        static const num ATK_UP = 202;
        static const num ATK_DOWN = 203;
        static const num DEF_UP = 204;
        static const num DEF_DOWN = 205;
        static const num SPD_UP = 206;
        static const num SPD_DOWN = 207;
        static const num MOV_UP = 208;
        static const num MOV_DOWN = 209;
        static const num POISON = 210;
        static const num POISON_HEAL = 211;
        static const num PARALYZE = 212;
        static const num PARALYZE_HEAL = 213;
        static const num STOP = 214;
        static const num STOP_HEAL = 215;
        static const num MAGIC_BLOCK = 216;
        static const num MAGIC_BLOCK_HEAL = 217;
        static const num MOVE_BLOCK = 218;
        static const num MOVE_BLOCK_HEAL = 219;
        static const num CURSE = 220;
        static const num CURSE_HEAL = 221;
        static const num ILLNES = 222;
        static const num ILLNES_HEAL = 223;
        static const num GIGA_VANISH = 224;
        static const num GIGA_VANISH_HEAL = 225;
    };

    struct TargetCode {
        /* Targets */
        static const num SINGLE_TARGET = 301;
        static const num RADIUS_TARGET = 302;
        static const num ALL_TARGET = 303;
    };

    struct ElemCode {
        /* Elements */
        static const num PHYSICAL = 401;
        static const num FIRE = 402;
        static const num WIND = 403;
        static const num WATER = 404;
        static const num EARTH = 405;
    };

    struct SpecialCode {
        /* Special Attacks */
        static const num HALVAH = 501;
        static const num VANISH = 502;
        static const num RESURRECT = 503;
    };

    struct GearCode {
        /* Gear */
        static const num WEAPON = 601;
        static const num HEAD_ARMOR = 602;
        static const num HAND_ARMOR = 603;
        static const num CHEST_ARMOR = 604;
        static const num FOOT_ARMOR = 605;
        static const num ACCESSOIRE = 606;
    };

    struct ErrorCode {
        /* Error */
        static const num ERROR_CODE_INPUT = 98;
        static const num ERROR_CODE_TARGET = 99;
    };

}

#endif
