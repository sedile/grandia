#include "Enemy.h"

using namespace grandia;

/**
 * @brief Enemy::Enemy  Enemy Constructor
 * @param name  Name of the enemy
 * @param mhp Maximum HP Value
 * @param mmp Maximum MP Value
 * @param msp Maximum SP Value
 * @param level Level of the enemy
 * @param maxXP Amount of XP what the Player gets when enemy is defeated
 * @param money Amount of Money what Player gets when enemy is defeated
 * @param str Strength
 * @param def Defense
 * @param spd Speed
 * @param mov Movement
 */
Enemy::Enemy(std::string name, num mhp, num mmp, num msp, num level, num maxXP, num money, num str, num def, num spd, num mov)
    : Role(name,false,mhp,mmp,msp,level,str,def,spd,mov), _xp(maxXP), _dropmoney(money) { }

/**
 * @brief Enemy::~Enemy Destructor
 */
Enemy::~Enemy() { }

/**
 * @brief Enemy::addDropItemToList Add items to the enemies droplist
 * @param item items what this enemy drops after the fight, if the player wins
 */
void Enemy::addDropItemToList(Item *item){
    _dropitems.push_back(item);
}

/**
 * @brief Enemy::resetEnemyXp resets the XP to 0 after the fight
 */
void Enemy::resetEnemyXp(){
    _xp = 0;
}

/**
 * @brief Enemy::resetEnemyMoney resets the Money to 0 after the fight
 */
void Enemy::resetEnemyMoney(){
    _dropmoney = 0;
}

/**
 * @brief Enemy::resetItems clears the items what the enemy drops
 */
void Enemy::resetItems(){
    _dropitems.clear();
}

/**
 * @brief Enemy::getEnemyXP returns the enemies XP
 * @return xp value
 */
num Enemy::getEnemyXP() const {
    return _xp;
}

/**
 * @brief Enemy::getEnemyMoney returns the enemies money
 * @return money value
 */
num Enemy::getEnemyMoney() const {
    return _dropmoney;
}

/**
 * @brief Enemy::dropItems returns the droplist of this enemy
 * @return list of items from the enemy
 */
std::vector<Item*>* Enemy::dropItems() {
    return &_dropitems;
}
