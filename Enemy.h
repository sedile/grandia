#ifndef ENEMY_H
#define ENEMY_H

#include <string>
#include <iostream>

#include "Role.h"
#include "Point.h"
#include "Status.h"
#include "SpellContainer.h"
#include "MoveContainer.h"

namespace grandia {

    class Enemy : public Role {
    private:
        num _xp, _dropmoney;
        std::vector<Item*> _dropitems;
    public:
        explicit Enemy(std::string name, num mhp, num mmp, num msp, num level, num maxXP, num money, num str, num def, num spd, num mov);
        virtual ~Enemy();

        void addDropItemToList(Item *item);
        void resetEnemyXp();
        void resetEnemyMoney();
        void resetItems();

        num getEnemyMoney() const;
        num getEnemyXP() const;
        std::vector<Item*>* dropItems();
    };

}

#endif // ENEMY_H
