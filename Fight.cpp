#include <iostream>
#include <typeinfo>

#include "Code.h"
#include "Fight.h"
#include "IPGauge.h"
#include "StandardItem.h"
#include "Enemy.h"
#include "Boss.h"
#include "Player.h"

using namespace grandia;

/**
 * @brief Fight::Fight Fight class
 * @param actors list of players and enemies in the battlefield
 * @param group only players are in the group
 */
Fight::Fight(std::vector<Role*> &actors, Group *group)
: _actors(actors), _group(group) { }

/**
 * @brief Fight::~Fight Destructor
 */
Fight::~Fight() { }

/**
 * @brief Fight::menuChooser if an item, spell or move should be used from a list
 * @return choosed number from the list
 */
num Fight::menuChooser(){
    char entry;
    printMessage("Choose a number : ");
    std::cin >> entry;
    if ( !(entry >= 48 && entry <= 57) ){
        return static_cast<num>(ErrorCode::ERROR_CODE_INPUT);
    } else {
        return entry - '0';
    }
}

/**
 * @brief Fight::commandInput choose an action from the 'wheel-menu'
 * @param idx the index of the actor from the actorlist
 * @return actiontype
 */
char Fight::commandInput(num &idx){
    char entry;

    std::cout << "[Menu]" << "\n";
    std::cout << "(c)ombo, (x)critical, (d)efense, (e)vade, (m)agic, (s)pecial, (i)nfo, (u)bag" << "\n";
    std::cin >> entry;

    Role *actor = _actors.at(idx);
    switch(entry) {
    case MenuCode::COMBO : {
        actor->updateActorsAction("Combo");
        return entry;
    }
    case MenuCode::CRITICAL : {
        actor->updateActorsAction("Critical");
        return entry;
    }
    case MenuCode::DEFEND : {
        actor->updateActorsAction("Defend");
        return entry;
    }
    case MenuCode::EVADE : {

        num x = actor->getActorPosition()->getX();
        num y = actor->getActorPosition()->getY();

        std::cout << "Actual Position (" << x << "," << y << ") \n";
        std::cout << "new x : ";
        std::cin >> _newx;
        std::cout << "new y : ";
        std::cin >> _newy;

        if ( x == _newx && y == _newy){
            return ErrorCode::ERROR_CODE_INPUT;
        }

        actor->updateActorsAction("Evade");
        return entry;
    }
    case MenuCode::MAGIC : {
        if ( actor->getStatus()->getMagicBlockFlag() ){
            printMessage("Magic Block : Magic can not be used!");
            return ErrorCode::ERROR_CODE_INPUT;
        }

        actor->getSpellContainer()->printMagicList();
        num number = menuChooser();
        if ( number >= actor->getSpellContainer()->getSize() ){
            printMessage("You entered a wrong index");
            return ErrorCode::ERROR_CODE_INPUT;
        }

        if ( actor->getSpellContainer()->getMagic(number)->getMPUse() > actor->getActMP() ){
            printMessage("Not enough Mana");
            return ErrorCode::ERROR_CODE_INPUT;
        }

        actor->chooseMagicAttack(actor->getSpellContainer()->getMagic(number));
        actor->updateActorsAction(actor->getChoosedMagic()->getName());
        return entry;
    }
    case MenuCode::SPECIAL : {
        if ( actor->getStatus()->getMoveBlockFlag() ){
            printMessage("Move Block : Moves can not be used!");
            return ErrorCode::ERROR_CODE_INPUT;
        }

        actor->getMoveContainer()->printMoveList();
        num number = menuChooser();
        if ( number >= actor->getMoveContainer()->getSize() ){
            printMessage("You entered a wrong index");
            return ErrorCode::ERROR_CODE_INPUT;
        }

        if ( actor->getMoveContainer()->getMove(number)->getSPUse() > actor->getActSP() ){
            printMessage("Not enough SP");
            return ErrorCode::ERROR_CODE_INPUT;
        }

        actor->chooseMoveAttack(actor->getMoveContainer()->getMove(number));
        actor->updateActorsAction(actor->getChoosedMove()->getName());
        return entry;
    }
    case MenuCode::INFO : {
        return entry;
    }
    case MenuCode::ITEM : {

        if ( !actor->isPlayer() ){
            return ErrorCode::ERROR_CODE_INPUT;
        }

        // we are a player and not an enemy
        _group->getItemBag()->printItemList();
        num number = menuChooser();

        if ( number >= _group->getItemBag()->getSize() ){
            printMessage("You entered a wrong index");
            return ErrorCode::ERROR_CODE_INPUT;
        }

        actor->chooseItem(_group->getItemBag()->getItem(number));
        dynamic_cast<Player*>(actor)->getPlayersGroup()->getItemBag()->deleteItem();
        return entry;
    }
    default : {
        printMessage("Unknow Action (Error)");
        return ErrorCode::ERROR_CODE_INPUT;
    }
    }
}

/**
 * @brief Fight::chooseTarget lists all enemies which can be a target
 * @param caster the index of the executor of an action
 * @return value from the list
 */
num Fight::chooseTarget(num &caster) {
    printMessage("Choose your target");

    for(num i = 0; i < _actors.size(); i++){
        if ( _actors.at(i)->getActHP() > 0 && _actors.at(i)->isPlayer() != _actors.at(caster)->isPlayer()){
            std::cout << "[" << std::to_string(i) << "] " << _actors.at(i)->getName() << "\n";
        }
    }

    num target = menuChooser();
    if ( target >= _actors.size() ){
        printMessage("You entered a wrong index");
        return ErrorCode::ERROR_CODE_TARGET;
    }    

    if ( _actors.at(caster)->isPlayer() != _actors.at(target)->isPlayer() ) {
        return target;
    } else {
        return ErrorCode::ERROR_CODE_TARGET;;
    }
}

/**
 * @brief Fight::chooseGroupTarget lists all own groupmembers for e.g. healing, buff
 * @param isPlayerGroup true, means actors are in the playergroup, false means actors are in the enemygroup
 * @return target as index
 */
num Fight::chooseGroupTarget(bool isPlayerGroup) {
    printMessage("Choose one of your Groupmember");

    if ( isPlayerGroup ){
        for(num i = 0; i < _actors.size(); i++){
            if ( _actors.at(i)->getActHP() > 0 && _actors.at(i)->isPlayer()){
                printMessage("[" + std::to_string(i) + "] " + _actors.at(i)->getName());
            }
        }
    } else {
        for(num i = 0; i < _actors.size(); i++){
            if ( _actors.at(i)->getActHP() > 0 && !_actors.at(i)->isPlayer()){
                printMessage("[" + std::to_string(i) + "] " + _actors.at(i)->getName());
            }
        }
    }

    num target = menuChooser();
    if ( target >= _actors.size() ){
        printMessage("You entered a wrong index");
        return ErrorCode::ERROR_CODE_TARGET;
    }    
    return target;
}

/**
 * @brief Fight::commandAction executes a previous given command
 * @param action the kind of an action (magic,combo,...)
 * @param caster the executor of the action
 * @param target the target from the executor
 */
void Fight::commandAction(char action, num &caster, num target){
    if( _actors.at(caster)->getStatus()->getCurseFlag() ){
        num random = std::rand();
        if ( random % 2 == 1 ){
            printMessage("Action failed because of Curse");
            return;
        }
    }

    switch(action) {
    case MenuCode::COMBO : {
        combo(caster, target);
        knockback(target, MenuCode::COMBO);
        break;
    }
    case MenuCode::CRITICAL : {
        critical(caster, target);
        knockback(target, MenuCode::CRITICAL);
        break;
    }
    case MenuCode::DEFEND : {
        defend(caster);
        break;
    }
    case MenuCode::EVADE : {
        evade(caster);
        break;
    }
    case MenuCode::MAGIC : {
        magic(caster, target);
        knockback(target, MenuCode::MAGIC);
        break;
    }
    case MenuCode::SPECIAL : {
        special(caster, target);
        knockback(target, MenuCode::SPECIAL);
        break;
    }
    case MenuCode::ITEM : {
        item(caster, target);
        break;
    }
    default : {
        printMessage("Unknown Action (Error)");
    }
    }
}

/**
 * @brief Fight::knockback simulates a knockback on the IP-Gauge
 * @param idx the victim of the knockback
 * @param type the attacktype, stronger attacks means stringer knockback
 */
void Fight::knockback(num &idx, char type){
    Role *actor = _actors.at(idx);
    if ( actor->getIP() < IPGauge::COM ){
        if ( type == MenuCode::CRITICAL) {
            actor->setIP(-250);
        } else if ( type == MenuCode::MAGIC ) {
            actor->setIP(-150);
        } else if ( type == MenuCode::COMBO ) {
            actor->setIP(-75);
        } else if ( type == MenuCode::SPECIAL ) {
            actor->setIP(-300);
        }
    } else if ( actor->getIP() > IPGauge::COM ) {
        if ( type == MenuCode::CRITICAL) {
            actor->setIP(-(actor->getIP() - IPGauge::COM) - 500);
            actor->setCastPhraseFlag(false);
            actor->updateActorsAction("Wait");

            /* reset item and place it back to the itembag becuase of critical attack against player */
            if ( actor->isPlayer() && actor->getChoosedItem() != nullptr ){
                Player *player = dynamic_cast<Player*>(actor);
                player->getPlayersGroup()->getItemBag()->addItem(actor->getChoosedItem());
                player->chooseItem(nullptr);
            }

            /* reset magic or move */
            if ( actor->getChoosedMagic() != nullptr ){
                actor->chooseMagicAttack(nullptr);
            } else if ( actor->getChoosedMove() != nullptr ){
                actor->chooseMoveAttack(nullptr);
            }

        } else if ( (type == MenuCode::MAGIC) || (type == MenuCode::COMBO) ) {
            actor->setIP(-75);
            if ( actor->getIP() <= IPGauge::COM ) {
                actor->setIP(-(actor->getIP() - IPGauge::COM) + 1);
            }
        } else if ( type == MenuCode::SPECIAL ) {
            actor->setIP(-(actor->getIP() - IPGauge::COM) + 1);

            /* reset item and place it back to the itembag becuase of critical attack against player */
            if ( actor->isPlayer() && actor->getChoosedItem() != nullptr ){
                Player *player = dynamic_cast<Player*>(actor);
                player->getPlayersGroup()->getItemBag()->addItem(actor->getChoosedItem());
                player->chooseItem(nullptr);
            }

            /* reset magic or move */
            if ( actor->getChoosedMagic() != nullptr ){
                actor->chooseMagicAttack(nullptr);
            } else if ( actor->getChoosedMove() != nullptr ){
                actor->chooseMoveAttack(nullptr);
            }
        }
    }
}

/**
 * @brief Fight::regenerateSP regenerates some SP
 * @param caster executor
 * @param target target of the executor
 */
void Fight::regenerateSP(num &caster, num &target){
    /* Get SP by hitting from enemy */
    if ( _actors.at(target)->getDefenseModeFlag() ) {
        _actors.at(target)->setSP(3);
    } else {
        _actors.at(target)->setSP(1);
    }
    /* Get SP by hitting the enemy */
    _actors.at(caster)->setSP(1);
}

/**
 * @brief Fight::addElementXp add xp for using magic or skill to player
 * @param actor xp added to this player
 */
void Fight::addElementXp(Role *actor){
    Player *player = dynamic_cast<Player*>(actor);
    num skill_xp = 5;

    /* Player used magic attack */
    if ( player->getChoosedMagic() != nullptr ){
        if ( player->getChoosedMagic()->getElement() == ElemCode::FIRE ){
            player->addFireXp(skill_xp);
        } else if ( player->getChoosedMagic()->getElement() == ElemCode::WIND ){
            player->addWindXp(skill_xp);
        } else if ( player->getChoosedMagic()->getElement() == ElemCode::WATER ){
            player->addWaterXp(skill_xp);
        } else if ( player->getChoosedMagic()->getElement() == ElemCode::EARTH ){
            player->addEarthXp(skill_xp);
        }
    /* Player uses special attack */
    } else if ( player->getChoosedMove() != nullptr ){
        if ( player->getChoosedMove()->getElement() == ElemCode::PHYSICAL ){
            player->addWeaponXp(skill_xp);
        } else if ( player->getChoosedMagic()->getElement() == ElemCode::FIRE ){
            player->addWeaponXp(skill_xp);
            player->addFireXp(skill_xp);
        } else if ( player->getChoosedMagic()->getElement() == ElemCode::WIND ){
            player->addWeaponXp(skill_xp);
            player->addWindXp(skill_xp);
        } else if ( player->getChoosedMagic()->getElement() == ElemCode::WATER ){
            player->addWeaponXp(skill_xp);
            player->addWaterXp(skill_xp);
        } else if ( player->getChoosedMagic()->getElement() == ElemCode::EARTH ){
            player->addWeaponXp(skill_xp);
            player->addEarthXp(skill_xp);
        }
    /* Player uses combo or critical */
    } else {
        player->addWeaponXp(skill_xp);
    }
}

/**
 * @brief Fight::calculateDamage calculated the damage in detail
 * @param caster executor
 * @param target target of the executor
 * @param resistence resistence value from the elements
 * @param effectlevel effectlevel from skills or spells
 * @return total damage
 */
num Fight::calculateDamage(num &caster, num &target, num &resistence, num &effectlevel) {
    num total_damage = 0;
    Role *acaster = _actors.at(caster);
    Role *atarget = _actors.at(target);
    if ( acaster->getChoosedMagic() != nullptr ) {
        num damage = static_cast<num>(acaster->getStrength() * 0.6f * effectlevel * 1.2f);
        num reduction = static_cast<num>(atarget->getDefense() * 1.2f * resistence);
        total_damage = damage - reduction;
    } else if ( acaster->getChoosedMove() != nullptr ){
        num damage = static_cast<num>(acaster->getStrength() * 0.6f * effectlevel * 1.2f);
        num reduction = static_cast<num>(atarget->getDefense() * 1.2f * resistence);
        total_damage = damage - reduction;
    }

    if ( total_damage <= 0 ) {
        return 0;
    } else {
        return total_damage;
    }
}

/**
 * @brief Fight::provideInfoForCalculate get informations about to claculate damage
 * @param caster executor
 * @param target target of the executor
 */
void Fight::provideInfoForCalculate(num &caster, num &target){
    Role *acaster = _actors.at(caster);
    Role *atarget = _actors.at(target);

    if( acaster->getChoosedMagic() != nullptr ){

        /* Uses MP */
        acaster->setMP(-(acaster->getChoosedMagic()->getMPUse()));
        printMessage(acaster->getName() + " uses " + acaster->getChoosedMagic()->getName());
        if ( acaster->getChoosedMagic()->getType() == AttackCode::ATTACK ) {
            if (acaster->getChoosedMagic()->getDistance() == TargetCode::SINGLE_TARGET ) {
                num target_resistence = examineTargetResistence(acaster->getChoosedMagic()->getElement(), target);
                num effectlevel = acaster->getChoosedMagic()->getEffectLevel();
                executeDamage(caster, target, target_resistence, effectlevel, 0);
            } else if ( acaster->getChoosedMagic()->getDistance() == TargetCode::RADIUS_TARGET ){
                for(num n = 0; n < _actors.size(); n++){
                    if ( _actors.at(n)->isPlayer() == atarget->isPlayer() ){
                        bool range = acaster->getActorPosition()->inRange(_actors.at(n)->getActorPosition());
                        if ( range ) {
                            num target_resistence = examineTargetResistence(acaster->getChoosedMagic()->getElement(), target);
                            num effectlevel = acaster->getChoosedMagic()->getEffectLevel();
                            executeDamage(caster, n, target_resistence, effectlevel, 0);
                        }
                    }
                }
            } else if ( acaster->getChoosedMagic()->getDistance() == TargetCode::ALL_TARGET ){
                for(num n = 0; n < _actors.size(); n++){
                    if ( _actors.at(n)->isPlayer() == atarget->isPlayer() ){
                        num target_resistence = examineTargetResistence(acaster->getChoosedMagic()->getElement(), target);
                        num effectlevel = acaster->getChoosedMagic()->getEffectLevel();
                        executeDamage(caster, n, target_resistence, effectlevel, 0);
                    }
                }
            }
        } else if ( acaster->getChoosedMagic()->getType() == AttackCode::BOOST || acaster->getChoosedMagic()->getType() == AttackCode::DECREASE ) {
            changeStatusAilment(caster, target, false);
        } else if ( acaster->getChoosedMagic()->getType() == AttackCode::HEAL ){
            if ( acaster->getChoosedMagic()->getDistance() == TargetCode::SINGLE_TARGET) {
                if ( caster == target ){
                    /* heal self */
                    num heal = 25 + acaster->getChoosedMagic()->getEffectLevel() * 4.5;
                    acaster->setHP(heal);
                    printMessage(acaster->getName() + " healed by " + std::to_string(heal) + "HP");
                } else {
                    /* heal other */
                    num heal = 25 + acaster->getChoosedMagic()->getEffectLevel() * 4.5;
                    atarget->setHP(heal);
                    printMessage(atarget->getName() + " healed by " + std::to_string(heal) + "HP");
                }
            } else if ( acaster->getChoosedMagic()->getDistance() == TargetCode::RADIUS_TARGET ) {
                for(num n = 0; n < _actors.size(); n++){
                    if( _actors.at(n)->isPlayer() == atarget->isPlayer() ){
                        bool range = acaster->getActorPosition()->inRange(_actors.at(n)->getActorPosition());
                        if ( range ) {
                            num heal = 25 + acaster->getChoosedMagic()->getEffectLevel() * 4.5;
                            _actors.at(n)->setHP(heal);
                            printMessage(_actors.at(n)->getName() + " healed by " + std::to_string(heal) + "HP");
                        }
                    }
                }
            } else if ( acaster->getChoosedMagic()->getDistance() == TargetCode::ALL_TARGET ){
                for(num n = 0; n < _actors.size(); n++){
                    if( _actors.at(n)->isPlayer() == atarget->isPlayer() ){
                        num heal = 25 + _actors.at(caster)->getChoosedMagic()->getEffectLevel() * 4.5;
                        _actors.at(n)->setHP(heal);
                        printMessage(_actors.at(n)->getName() + " healed by " + std::to_string(heal) + "HP");
                    }
                }
            } 
        } else if ( acaster->getChoosedMagic()->getType() == SpecialCode::HALVAH){
            healStatusAilments(target);
            acaster->chooseMagicAttack(nullptr);
        } else if ( acaster->getChoosedMagic()->getType() == SpecialCode::RESURRECT){
            resurrectTarget(target);
            acaster->chooseMagicAttack(nullptr);
        } else if ( acaster->getChoosedMagic()->getType() == SpecialCode::VANISH){
            vanishEffect();
            acaster->chooseMagicAttack(nullptr);
        }
        acaster->chooseMagicAttack(nullptr);
        return;
    } else if( acaster->getChoosedMove() != nullptr ){

        /* Uses SP */
        acaster->setSP(-(acaster->getChoosedMove()->getSPUse()));
        printMessage(acaster->getName() + " uses " + acaster->getChoosedMove()->getName());
        if ( acaster->getChoosedMove()->getType() == AttackCode::ATTACK ){
            if ( acaster->getChoosedMove()->getDistance() == TargetCode::SINGLE_TARGET ) {
                num target_resistence = examineTargetResistence(acaster->getChoosedMove()->getElement(), target);
                num effectlevel = acaster->getChoosedMove()->getEffectLevel();
                executeDamage(caster, target, target_resistence, effectlevel, 0);
            } else if ( acaster->getChoosedMove()->getDistance() == TargetCode::RADIUS_TARGET ){
                for(num n = 0; n < _actors.size(); n++){
                    if ( _actors.at(n)->isPlayer() == atarget->isPlayer() ){
                        bool range = acaster->getActorPosition()->inRange(_actors.at(n)->getActorPosition());
                        if ( range ) {
                            num target_resistence = examineTargetResistence(acaster->getChoosedMove()->getElement(), target);
                            num effectlevel = acaster->getChoosedMove()->getEffectLevel();
                            executeDamage(caster, n, target_resistence, effectlevel, 0);
                        }
                    }                        
                }       
            } else if ( acaster->getChoosedMove()->getDistance() == TargetCode::ALL_TARGET ){
                for(num n = 0; n < _actors.size(); n++){
                    if ( _actors.at(n)->isPlayer() == atarget->isPlayer() ){
                        num target_resistence = examineTargetResistence(acaster->getChoosedMove()->getElement(), target);
                        num effectlevel = acaster->getChoosedMove()->getEffectLevel();
                        executeDamage(caster, n, target_resistence, effectlevel, 0);
                    } 
                }  
            }
        }
        acaster->chooseMoveAttack(nullptr);
        return;
    } else if( acaster->getChoosedItem() != nullptr ){

        printMessage(acaster->getName() + " uses " + acaster->getChoosedItem()->getName());
        StandardItem *standard = dynamic_cast<StandardItem*>(acaster->getChoosedItem());

        if ( standard->getType() == AttackCode::ATTACK) {
            if ( standard->getDistance() == TargetCode::SINGLE_TARGET) {
                num target_resistence = examineTargetResistence(standard->getElement(), target);
                num itemvalue = standard->getValue();
                executeDamage(caster, target, target_resistence, 1, itemvalue);
            } else if ( standard->getDistance() == TargetCode::RADIUS_TARGET ) {
                for(num n = 0; n < _actors.size(); n++){
                    if ( _actors.at(n)->isPlayer() == atarget->isPlayer() ){
                        bool range = acaster->getActorPosition()->inRange(_actors.at(n)->getActorPosition());
                        if ( range ) {
                            num target_resistence = examineTargetResistence(standard->getElement(), target);
                            num itemvalue = standard->getValue();
                            executeDamage(caster, n, target_resistence, 1, itemvalue);
                        }
                    }
                }
            } else if ( standard->getDistance() == TargetCode::ALL_TARGET ) {
                for(num n = 0; n < _actors.size(); n++){
                    if ( _actors.at(n)->isPlayer() == atarget->isPlayer() ){
                        num target_resistence = examineTargetResistence(standard->getElement(), target);
                        num itemvalue = standard->getValue();
                        executeDamage(caster, n, target_resistence, 1, itemvalue);
                    }
                }
            }
        } else if ( standard->getType() == AttackCode::BOOST || standard->getType() == AttackCode::DECREASE ) {
            changeStatusAilment(caster, target, true);
        } else if ( standard->getType() == AttackCode::HEAL ){
            if ( standard->getDistance() == TargetCode::SINGLE_TARGET ) {
                atarget->setHP(standard->getValue());
                printMessage(atarget->getName() + " healed by " + std::to_string(standard->getValue()) + "HP");
            } else if ( standard->getDistance() == TargetCode::RADIUS_TARGET ) {
                for(num n = 0; n < _actors.size(); n++){
                    if( _actors.at(n)->isPlayer() == atarget->isPlayer() ){
                        bool range = acaster->getActorPosition()->inRange(_actors.at(n)->getActorPosition());
                        if ( range ) {
                            _actors.at(n)->setHP(standard->getValue());
                            printMessage(_actors.at(n)->getName() + " healed by " + std::to_string(standard->getValue()) + "HP");
                        }
                    }
                }
            } else if ( standard->getDistance() == TargetCode::ALL_TARGET ){
                for(num n = 0; n < _actors.size(); n++){
                    if( _actors.at(n)->isPlayer() == atarget->isPlayer() ){
                        _actors.at(n)->setHP(standard->getValue());
                        printMessage(_actors.at(n)->getName() + " healed by " + std::to_string(standard->getValue()) + "HP");
                    }
                }
            }
        }

        acaster->chooseItem(nullptr);
        //dynamic_cast<Player*>(acaster)->getPlayersGroup()->getItemBag()->deleteItem();
    } 
}

/**
 * @brief Fight::examineTargetResistence examines the type of resistence of the target
 * @param element element from the spell or skill
 * @param target target of the executor
 * @return elementvalue
 */
num Fight::examineTargetResistence(num element, num &target){
    if ( element == ElemCode::FIRE ){
        return _actors.at(target)->getFireResistence();
    } else if ( element == ElemCode::WIND ){
        return _actors.at(target)->getWindResistence();
    } else if ( element == ElemCode::WATER ){
        return _actors.at(target)->getWaterResistence();
    } else if ( element == ElemCode::EARTH ){
        return _actors.at(target)->getEarthResistence();
    } else {
        return 1; // normal resistence
    }
}

/**
 * @brief Fight::executeDamage does damage to targets
 * @param caster executor
 * @param target target of the exector
 * @param resistence resistence value of the element (0 = immun, 1 = standard, 2 = effective)
 * @param effectlevel effectlevel from the spell or skill
 * @param itemvalue greater 0 means item, 0 means no item
 */
void Fight::executeDamage(num &caster, num &target, num resistence, num effectlevel, num itemvalue){
    /* basedamage */
    short dmg = -50;
    if ( itemvalue > 0 ){
        /* only for items, make 'itemdamage' */
        _actors.at(target)->setHP(-itemvalue);
        printMessage(_actors.at(caster)->getName() + " inflict " + _actors.at(target)->getName() + " " + std::to_string(itemvalue) + " Damage.");
        return;
    }

    /* if target is immun */
    if ( resistence == 0){
        _actors.at(target)->setHP(dmg);
        regenerateSP(caster, target);
        printMessage(_actors.at(target)->getName() + " takes no damage (Immun)");
        return;
    }

    dmg -= calculateDamage(caster, target, resistence, effectlevel);
    if ( _actors.at(target)->getDefenseModeFlag() ){
        dmg /= 2;
    }

    _actors.at(target)->setHP(dmg);
    regenerateSP(caster, target);
    printMessage(_actors.at(caster)->getName() + " inflict " + _actors.at(target)->getName() + " " + std::to_string(dmg) + " Damage ");
    if ( _actors.at(target)->getActHP() == 0 ){
        if ( _actors.at(target)->isPlayer() ){
            printMessage(_actors.at(target)->getName() + " is defeated");
        } else {
            try {
                Enemy *enemy = dynamic_cast<Enemy*>(_actors.at(target));
                _group->setMoney(enemy->getEnemyMoney());
                _group->setXp(enemy->getEnemyXP());
                _group->addItemsFromFight(enemy->dropItems());
                enemy->resetEnemyMoney();
                enemy->resetEnemyXp();
                enemy->resetItems(); // delete enemies items
                printMessage(enemy->getName() + " is defeated.");
            } catch ( std::bad_cast &e ){
                Boss *boss = dynamic_cast<Boss*>(_actors.at(target));

                /* If mainpart, defeat all other bossparts */
                if ( boss->isMainpart() ){
                    for(num i = 0; i < _actors.size(); i++){
                        if( !_actors.at(i)->isPlayer() ){
                            _actors.at(i)->setHP(-_actors.at(i)->getActHP());
                            _group->setMoney(dynamic_cast<Boss*>(_actors.at(i))->getEnemyMoney());
                            _group->setXp(dynamic_cast<Boss*>(_actors.at(i))->getEnemyXP());
                            _group->addItemsFromFight(dynamic_cast<Boss*>(_actors.at(i))->dropItems());
                            dynamic_cast<Boss*>(_actors.at(i))->resetEnemyMoney();
                            dynamic_cast<Boss*>(_actors.at(i))->resetEnemyXp();
                            dynamic_cast<Boss*>(_actors.at(i))->resetItems();
                            printMessage(boss->getName() + " is defeated.");
                        }
                    }
                }
            }
        }
    }
}

/**
 * @brief Fight::combo simulates a combo attack, 2 attacks
 * @param caster executor
 * @param target target of the executor
 */
void Fight::combo(num &caster, num &target){

    // (Angreifer, Ziel, Elementresistenz, Effektlevel, Itemwert)
    printMessage(_actors.at(caster)->getName() + " uses Combo.");
    executeDamage(caster, target, 1, 1, 0);
    executeDamage(caster, target, 1, 1, 0);

    if ( _actors.at(caster)->isPlayer() ){
        addElementXp(_actors.at(caster));
        addElementXp(_actors.at(caster));
    }
}

/**
 * @brief Fight::critical Simulates a Critical-Attack
 * @param caster The Executer of the Criticalattack
 * @param target The receiver of the damage
 */
void Fight::critical(num &caster, num &target){
    printMessage(_actors.at(caster)->getName() + " uses Critical.");
    executeDamage(caster, target, 2, 1, 0);

    if ( _actors.at(caster)->isPlayer() ){
        addElementXp(_actors.at(caster));
    }
}

/**
 * @brief Fight::defend Simulates a defenseprogress
 * @param caster The Defender
 */
void Fight::defend(num &caster) {
    printMessage(_actors.at(caster)->getName() + " will defend in the next turn");
    _actors.at(caster)->setDefenseMode(true);
}

void Fight::evade(num &caster) {
    printMessage(_actors.at(caster)->getName() + " changes position on battlefield");

    /* Go to this position */
    _actors.at(caster)->updateActorPosition(_newx,_newy);
}

/**
 * @brief Fight::magic Simulates a Magic-Attack
 * @param caster The Executer of the Magicattack
 * @param target The receiver of the damage
 */
void Fight::magic(num &caster, num &target){
    provideInfoForCalculate(caster, target);

    if ( _actors.at(caster)->isPlayer() ){
        addElementXp(_actors.at(caster));
    }
}

/**
 * @brief Fight::item Simulates the choosen Item from an Actor
 * @param caster The Actor's ID
 * @param target The target for the item
 */
void Fight::item(num &caster, num &target){
    provideInfoForCalculate(caster, target);
}

/**
 * @brief Fight::special Simulates a Special-Attack (Move)
 * @param caster The Executer of the Specialattack
 * @param target The receiver of the damage
 */
void Fight::special(num &caster, num &target){
    provideInfoForCalculate(caster, target);

    if ( _actors.at(caster)->isPlayer() ){
        addElementXp(_actors.at(caster));
    }
}

/**
 * @brief Fight::changeStatusAilment Simulates all Status-Ailments
 * @param caster The Executer of the Attack
 * @param target The receiver of the Attack which inflicts a bad status
 * @param isItem true, is an item, false, is a magic
 */
void Fight::changeStatusAilment(num &caster, num &target, bool isItem){
    Role *acaster = _actors.at(caster);
    Role *atarget = _actors.at(target);
    
    num flag;
    if ( isItem ){
        flag = dynamic_cast<StandardItem*>(acaster->getChoosedItem())->getStatusFlag();
    } else {
        flag = acaster->getChoosedMagic()->getStatusFlag();
    }

    switch(flag) {
    case StatusCode::POISON_HEAL : {
        atarget->getStatus()->setPoisonFlag(false);
        printMessage(atarget->getName() + " was cured of poison.");
        break;
    }
    case StatusCode::POISON : {
        atarget->getStatus()->setPoisonFlag(true);
        printMessage(atarget->getName() + " is poisoned.");
        break;
    }
    case StatusCode::PARALYZE_HEAL : {
        atarget->getStatus()->setParalyzeFlag(false);
        printMessage(atarget->getName() + " was cured of paralyze.");
        break;
    }
    case StatusCode::PARALYZE : {
        atarget->getStatus()->setParalyzeFlag(true);
        printMessage(atarget->getName() + " is paralyzed.");
        break;
    }
    case StatusCode::STOP_HEAL : {
        atarget->getStatus()->setStopFlag(false);
        printMessage(atarget->getName() + " can act now.");
        break;
    }
    case StatusCode::STOP : {
        atarget->getStatus()->setStopFlag(true);
        printMessage(atarget->getName() + " is unable to act (Stop).");
        break;
    }
    case StatusCode::MAGIC_BLOCK_HEAL : {
        atarget->getStatus()->setMagicBlockFlag(false);
        printMessage("The Magicblock of " + atarget->getName() + " was cured.");
        break;
    }
    case StatusCode::MAGIC_BLOCK : {
        atarget->getStatus()->setMagicBlockFlag(true);
        printMessage(atarget->getName() + " gets a Magicblock.");
        break;
    }
    case StatusCode::MOVE_BLOCK_HEAL : {
        atarget->getStatus()->setMoveBlockFlag(false);
        printMessage("The Mveblock of " + atarget->getName() + " was cured.");
        break;
    }
    case StatusCode::MOVE_BLOCK : {
        atarget->getStatus()->setMoveBlockFlag(true);
        printMessage(atarget->getName() + " gets a Moveblock.");
        break;
    }
    case StatusCode::CURSE_HEAL : {
        atarget->getStatus()->setCurseFlag(false);
        printMessage("The Curse of " + atarget->getName() + " is gone.");
        break;
    }
    case StatusCode::CURSE : {
        atarget->getStatus()->setCurseFlag(true);
        printMessage(atarget->getName() + " is Cursed.");
        break;
    }
    case StatusCode::ILLNES_HEAL : {
        atarget->getStatus()->setIllnesFlag(false);
        printMessage("The Illnes of " + atarget->getName() + " was cured.");
        break;
    }
    case StatusCode::ILLNES : {
        _actors.at(target)->getStatus()->setIllnesFlag(true);
        printMessage(_actors.at(target)->getName() + " is seriously Ill.");
        break;
    }
    case StatusCode::GIGA_VANISH_HEAL : {
        printMessage("The Giga Vanish effect of " + atarget->getName() + " is over.");
        atarget->getStatus()->setGigaVanishFlag(false);
        break;
    }
    case StatusCode::GIGA_VANISH : {
        atarget->getStatus()->setGigaVanishFlag(true);
        printMessage(atarget->getName() + " is affected by Giga Vanish");
        break;
    }
    case StatusCode::ATK_UP : {
        if ( atarget->getStatus()->getAttackBoost() < 5){
            atarget->getStatus()->setAttackBoost(1);
            num modvalue = atarget->getOriginStrength() * 0.1f;
            atarget->setStrength(atarget->getStrength() + modvalue);
        }
        printMessage(atarget->getName() + " : AKT-Level " + std::to_string(atarget->getStatus()->getAttackBoost()));
        break;
    }
    case StatusCode::ATK_DOWN : {
        if ( atarget->getStatus()->getAttackBoost() > -5 ){
            atarget->getStatus()->setAttackBoost(-1);
            num modvalue = atarget->getOriginStrength() * 0.1f;
            atarget->setStrength(atarget->getStrength() - modvalue);
        }
        printMessage(atarget->getName() + " : AKT-Level " + std::to_string(atarget->getStatus()->getAttackBoost()));
        break;
    }
    case StatusCode::DEF_UP : {
        if ( atarget->getStatus()->getDefenseBoost() < 5){
            atarget->getStatus()->setDefenseBoost(1);
            num modvalue = atarget->getOriginDefense() * 0.1f;
            atarget->setDefense(atarget->getDefense() + modvalue);
        }
        printMessage(atarget->getName() + " : DEF-Level " + std::to_string(atarget->getStatus()->getDefenseBoost()));
        break;
    }
    case StatusCode::DEF_DOWN : {
        if ( atarget->getStatus()->getDefenseBoost() > -5 ){
            atarget->getStatus()->setDefenseBoost(-1);
            num modvalue = atarget->getOriginDefense() * 0.1f;
            atarget->setDefense(atarget->getDefense() - modvalue);
        }
        printMessage(atarget->getName() + " : DEF-Level " + std::to_string(atarget->getStatus()->getDefenseBoost()));
        break;
    }
    case StatusCode::SPD_UP : {
        if ( atarget->getStatus()->getSpeedBoost() < 5){
            atarget->getStatus()->setSpeedBoost(1);
            num modvalue = atarget->getOriginSpeed() * 0.1f;
            atarget->setSpeed(atarget->getSpeed() + modvalue);
        }
        printMessage(atarget->getName() + " : SPD-Level " + std::to_string(atarget->getStatus()->getSpeedBoost()));
        break;
    }
    case StatusCode::SPD_DOWN : {
        if ( atarget->getStatus()->getSpeedBoost() > -5 ){
            atarget->getStatus()->setSpeedBoost(-1);
            num modvalue = atarget->getOriginSpeed() * 0.1f;
            atarget->setSpeed(atarget->getSpeed() - modvalue);
        }
        printMessage(atarget->getName() + " : SPD-Level " + std::to_string(atarget->getStatus()->getSpeedBoost()));
        break;
    }
    case StatusCode::MOV_UP : {
        if ( atarget->getStatus()->getMoveBoost() < 5){
            atarget->getStatus()->setMoveBoost(1);
            num modvalue = atarget->getOriginMove() * 0.1f;
            atarget->setMove(atarget->getMove() + modvalue);
        }
        printMessage(atarget->getName() + " : MOV-Level " + std::to_string(atarget->getStatus()->getMoveBoost()));
        break;
    }
    case StatusCode::MOV_DOWN : {
        if ( atarget->getStatus()->getMoveBoost() > -5 ){
            atarget->getStatus()->setMoveBoost(-1);
            num modvalue = atarget->getOriginMove() * 0.1f;
            atarget->setMove(atarget->getMove() - modvalue);
        }
        printMessage(atarget->getName() + " : MOV-Level " + std::to_string(atarget->getStatus()->getMoveBoost()));
        break;
    }
    default : { }
    }
}

/**
 * @brief Fight::showInfo Shows a Statuspanel of one Actor
 */
void Fight::showInfo(){
    printMessage("Statusinfo from whom?");
    for(num n = 0; n < _actors.size(); n++){
        if( _actors.at(n)->getActHP() > 0 ){
            std::cout << "[" << n << "] " << _actors.at(n)->getName() << "\n";
        }
    }
    num info = menuChooser();
    if ( info >= _actors.size() ){
        printMessage("You entered a wrong index");
        return;
    }

    Role *actor = _actors.at(info);
    std::cout << "[STATUSPANEL]" << "\n";
    std::cout << actor->getName() << "\n";
    std::cout << "HP : " << actor->getActHP() << " / " << actor->getMaxHP() << " ";
    std::cout << "MP : " <<  actor->getActMP() << " / " << actor->getMaxMP() << " ";
    std::cout << "SP : " <<  actor->getActSP() << " / " << actor->getMaxSP() << "\n";
    std::cout << "STR : " << actor->getStrength() << " ";
    std::cout << "DEF : " << actor->getDefense() << " ";
    std::cout << "SPD : " << actor->getSpeed() << " ";
    std::cout << "MOV : " << actor->getMove() << " ";
    std::cout << "IP : " << actor->getIP() << "\n";
    std::cout << "FIRE : " << std::to_string(actor->getFireResistence()) << " ";
    std::cout << "WIND : " << std::to_string(actor->getWindResistence()) << " ";
    std::cout << "WATER : " << std::to_string(actor->getWaterResistence()) << " ";
    std::cout << "EARTH : " << std::to_string(actor->getEarthResistence()) << "\n";
    std::cout << "Position : " << std::to_string(actor->getActorPosition()->getX()) << " " << std::to_string(actor->getActorPosition()->getY()) << "\n";
    std::cout << "---------------------------------" << "\n";
    std::cout << "Action : " << actor->getActorsAction() << " ---> ";
    std::cout << _actors.at(actor->getTarget())->getName() << "\n";
    actor->getStatus()->showStatus();
}

/**
 * @brief Fight::printMessage helpfunction to print strings to console
 * @param msg
 */
void Fight::printMessage(std::string msg){
    std::cout << msg << "\n";
}

/**
 * @brief Fight::getFightStatus To examine if the fight is over. If all members in one
 * group is defeated, the fight is over
 * @return 0 = Fight continues, 1 = Playergroup wins, 2 = Enemygroup wins
 */
num Fight::getFightStatus() const {
    std::vector<Role*> ptemp, etemp;
    for(num i = 0; i < _actors.size(); i++){
        if ( _actors.at(i)->isPlayer() ){
            ptemp.push_back(_actors.at(i));
        } else {
            etemp.push_back(_actors.at(i));
        }
    }

    num gsize = ptemp.size();
    for(num j = 0; j < ptemp.size(); j++){
        if ( ptemp.at(j)->getActHP() == 0 ){
            gsize--;
        }
    }
    
    if( gsize == 0 ){
        return 1;
    }

    gsize = etemp.size();
    for(num j = 0; j < etemp.size(); j++){
        if ( etemp.at(j)->getActHP() == 0 ){
            gsize--;
        }
    }   

    if ( gsize == 0){
        return 2;
    }

    return 0;
}

/**
 * @brief Fight::healStatus function for the spell 'Halvah'
 * @param target target
 */
void Fight::healStatusAilments(num &target) {
    _actors.at(target)->getStatus()->resetAilments();
    printMessage(_actors.at(target)->getName() + " Status Ailments are healed");
}

/**
 * @brief Fight::resurrect function for the spell 'Resurrect'
 * @param target target
 */
void Fight::resurrectTarget(num &target) {
    if ( _actors.at(target)->getActHP() == 0 ){
        _actors.at(target)->setHP(_actors.at(target)->getMaxHP());
        printMessage(_actors.at(target)->getName() + " lives again");
    }
}

/**
 * @brief Fight::vanishEffect activates the vanish effect (enemy only)
 * @param caster caster from the vanish spell
 */
void Fight::vanishEffect(){
    for(num i = 0; i < _actors.size(); i++){
        /* 1. reset all parameters to 0 for everyone */
        _actors.at(i)->getStatus()->resetBoost();
    }

    for(num i = 0; i < _actors.size(); i++){
        if ( !_actors.at(i)->isPlayer() ){
            /* Heal Status ailments only from enemies */
            healStatusAilments(i);
        }
    }
}
