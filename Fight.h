#ifndef __FIGHT_H__
#define __FIGHT_H__

#include <vector>
#include "Group.h"

namespace grandia {

    class Fight {
    private:
        std::vector<Role*> _actors;
        Group *_group;
        num _newx, _newy;

        void combo(num &caster, num &target);
        void critical(num &caster, num &target);
        void defend(num &caster);
        void evade(num &caster);
        void magic(num &caster, num &target);
        void special(num &caster, num &target);
        void item(num &caster, num &target);
        void knockback(num &idx, char type);
        void provideInfoForCalculate(num &caster, num &target);
        num examineTargetResistence(num element, num &target);
        void examineResistence();
        void executeDamage(num &caster, num &target, num elemtype, num effectlevel, num itemvalue);
        void changeStatusAilment(num &caster, num &target, bool isItem);
        void regenerateSP(num &caster, num &target);
        num calculateDamage(num &caster, num &target, num &elemtype, num &el);
        void addElementXp(Role *player);
        void healStatusAilments(num &target);
        void resurrectTarget(num &target);
        void vanishEffect();
    public:
        explicit Fight(std::vector<Role*> &actors, Group *group);
        virtual ~Fight();

        num menuChooser();
        num chooseTarget(num &caster);
        num chooseGroupTarget(bool isPlayerGroup);
        char commandInput(num &idx);
        void commandAction(char actiontype, num &caster, num target);
        void showInfo();
        void printMessage(std::string msg);
        num getFightStatus() const;
    };

}

#endif
