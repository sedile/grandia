#include "Gear.h"
#include "Code.h"

using namespace grandia;

/**
 * @brief Gear::Gear A class for the Players equipment
 */
Gear::Gear() { }

/**
 * @brief Gear::~Gear Destructor. Deletes all pieces of equipment
 */
Gear::~Gear() {
    delete _weapon;
    delete _head;
    delete _hand;
    delete _chest;
    delete _foot;
    delete _accessoire;
}

/**
 * @brief Gear::addWeapon add a weapon to player
 * @param weapon weapon
 */
void Gear::addWeapon(GearItem *weapon){
    if ( weapon != nullptr ){
        _weapon = weapon;
    }
}

/**
 * @brief Gear::addHeadArmor add a head armor to player
 * @param helmarmor headarmor
 */
void Gear::addHeadArmor(GearItem *helmarmor){
    if ( helmarmor != nullptr ){
        _head = helmarmor;
    }
}

/**
 * @brief Gear::addHandArmor add a glove to player
 * @param handarmor glove or handarmor
 */
void Gear::addHandArmor(GearItem *handarmor){
    if ( handarmor != nullptr ){
        _hand = handarmor;
    }
}

/**
 * @brief Gear::addChestArmor add a body armor or jacket to player
 * @param chestarmor chestarmor
 */
void Gear::addChestArmor(GearItem *chestarmor){
    if ( chestarmor != nullptr ){
        _chest = chestarmor;
    }
}

/**
 * @brief Gear::addFootArmor add boots to player
 * @param footarmor footarmor
 */
void Gear::addFootArmor(GearItem *footarmor){
    if ( footarmor != nullptr ){
        _foot = footarmor;
    }
}

/**
 * @brief Gear::addAccessoire add a ring or necklace to player
 * @param accessoire accessoire
 */
void Gear::addAccessoire(GearItem *accessoire){
    if ( accessoire != nullptr ){
        _accessoire = accessoire;
    }
}

/**
 * @brief Gear::getWeapon get actual weapon
 * @return weapon
 */
GearItem* Gear::getWeapon(){
    return _weapon;
}

/**
 * @brief Gear::getHeadArmor get actual head armor
 * @return head armor
 */
GearItem* Gear::getHeadArmor(){
    return _head;
}

/**
 * @brief Gear::getHandArmor get actual glove
 * @return handarmor
 */
GearItem* Gear::getHandArmor(){
    return _hand;
}

/**
 * @brief Gear::getChestArmor get actual body armor
 * @return chestarmor
 */
GearItem* Gear::getChestArmor(){
    return _chest;
}

/**
 * @brief Gear::getFootArmor get actual boots
 * @return footarmor
 */
GearItem* Gear::getFootArmor(){
    return _foot;
}

/**
 * @brief Gear::getAccessoire get actual accessoire
 * @return accessoire
 */
GearItem* Gear::getAccessoire(){
    return _accessoire;
}

/**
 * @brief Gear::getBonusValues calculates bonusvalues from all players equipment
 * @param bonusvector a referencevector which collects all bonusvalues
 */
void Gear::getBonusValues(std::vector<short> &bonusvector){
    short atk = 0;
    short def = 0;
    short spd = 0;
    short mov = 0;
    if ( getWeapon() != nullptr ){
        switch(getWeapon()->getEffectType()) {
        case StatusCode::ATK_UP : {
            atk += getWeapon()->getValue();
            break;
        }
        case StatusCode::DEF_UP : {
            def += getWeapon()->getValue();
            break;
        }
        case StatusCode::SPD_UP : {
            spd += getWeapon()->getValue();
            break;
        }
        case StatusCode::MOV_UP : {
            mov += getWeapon()->getValue();
            break;
        }
        }
    }

    if ( getHeadArmor() != nullptr ){
        switch(getHeadArmor()->getEffectType()) {
        case StatusCode::ATK_UP : {
            atk += getHeadArmor()->getValue();
            break;
        }
        case StatusCode::DEF_UP : {
            def += getHeadArmor()->getValue();
            break;
        }
        case StatusCode::SPD_UP : {
            spd += getHeadArmor()->getValue();
            break;
        }
        case StatusCode::MOV_UP : {
            mov += getHeadArmor()->getValue();
            break;
        }
        }
    }

    if ( getHandArmor() != nullptr ){
        switch(getHandArmor()->getEffectType()) {
        case StatusCode::ATK_UP : {
            atk += getHandArmor()->getValue();
            break;
        }
        case StatusCode::DEF_UP : {
            def += getHandArmor()->getValue();
            break;
        }
        case StatusCode::SPD_UP : {
            spd += getHandArmor()->getValue();
            break;
        }
        case StatusCode::MOV_UP : {
            mov += getHandArmor()->getValue();
            break;
        }
        }
    }

    if ( getChestArmor() != nullptr ){
        switch(getChestArmor()->getEffectType()) {
        case StatusCode::ATK_UP : {
            atk += getChestArmor()->getValue();
            break;
        }
        case StatusCode::DEF_UP : {
            def += getChestArmor()->getValue();
            break;
        }
        case StatusCode::SPD_UP : {
            spd += getChestArmor()->getValue();
            break;
        }
        case StatusCode::MOV_UP : {
            mov += getChestArmor()->getValue();
            break;
        }
        }
    }

    if ( getFootArmor() != nullptr ){
        switch(getFootArmor()->getEffectType()) {
        case StatusCode::ATK_UP : {
            atk += getFootArmor()->getValue();
            break;
        }
        case StatusCode::DEF_UP : {
            def += getFootArmor()->getValue();
            break;
        }
        case StatusCode::SPD_UP : {
            spd += getFootArmor()->getValue();
            break;
        }
        case StatusCode::MOV_UP : {
            mov += getFootArmor()->getValue();
            break;
        }
        }
    }

    if ( getAccessoire() != nullptr ){
        switch(getAccessoire()->getEffectType()) {
        case StatusCode::ATK_UP : {
            atk += getAccessoire()->getValue();
            break;
        }
        case StatusCode::DEF_UP : {
            def += getAccessoire()->getValue();
            break;
        }
        case StatusCode::SPD_UP : {
            spd += getAccessoire()->getValue();
            break;
        }
        case StatusCode::MOV_UP : {
            mov += getAccessoire()->getValue();
            break;
        }
        }
    }

    bonusvector.push_back(atk);
    bonusvector.push_back(def);
    bonusvector.push_back(spd);
    bonusvector.push_back(mov);
}
