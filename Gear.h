#ifndef GEAR_H
#define GEAR_H

#include "GearItem.h"

namespace grandia {

class Gear {
private:
    GearItem *_weapon = nullptr;
    GearItem *_head = nullptr;
    GearItem *_chest = nullptr;
    GearItem *_hand = nullptr;
    GearItem *_foot = nullptr;
    GearItem *_accessoire = nullptr;
public:
    explicit Gear();
    virtual ~Gear();

    void addWeapon(GearItem *weapon);
    void addHeadArmor(GearItem *helmarmor);
    void addChestArmor(GearItem *chestarmor);
    void addHandArmor(GearItem *handarmor);
    void addFootArmor(GearItem *footarmor);
    void addAccessoire(GearItem *accessoire);

    GearItem* getWeapon();
    GearItem* getHeadArmor();
    GearItem* getChestArmor();
    GearItem* getHandArmor();
    GearItem* getFootArmor();
    GearItem* getAccessoire();
    void getBonusValues(std::vector<short> &bonusvector);
};

}

#endif // GEAR_H
