#include "GearItem.h"

using namespace grandia;

/**
 * @brief GearItem::GearItem Items what can only be used to equip player or can be selled
 * @param isStandardItem must be false
 * @param name Name of this item
 * @param desc Description of this item, should not be empty
 * @param value Value of this item, ex. if this item should effect +20 STR, then the value should be 20
 * @param geartype what kind of gear (head,chest,...)
 * @param effecttype what this item should effect, ex. DEF_UP
 * @param elem baseelement of this item
 * @param item_value value of this item for vendors
 */
GearItem::GearItem(bool isStandardItem, std::string name, std::string desc, num value, num geartype, num effecttype, num elem, num item_value)
    : Item(isStandardItem,name,desc,item_value), VALUE(value), GEAR_TYPE(geartype), EFFECT_TYPE(effecttype), ELEMENT(elem) { }

/**
 * @brief GearItem::~GearItem Destructor
 */
GearItem::~GearItem() { }

/**
 * @brief GearItem::getValue Returns the Value of an Item. This describes how many
 * damage this item will cause, or how much HP will be restored
 * @return the Item Value
 */
num GearItem::getValue() const {
    return VALUE;
}

/**
 * @brief GearItem::getElement returns the elementtype of this item
 * @return elementtype
 */
num GearItem::getElement() const {
    return ELEMENT;
}

/**
 * @brief GearItem::getGearType returns the type of this gearitem (Head, Hand,...)
 * @return itemtype
 */
num GearItem::getGearType() const {
    return GEAR_TYPE;
}

/**
 * @brief GearItem::getEffectType returns the effecttype
 * @return effecttype
 */
num GearItem::getEffectType() const {
    return EFFECT_TYPE;
}
