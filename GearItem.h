#ifndef GEARITEM_H
#define GEARITEM_H

#include <string>

#include "Item.h"

namespace grandia {

    class GearItem : public Item {
    private:
        const num VALUE, GEAR_TYPE, EFFECT_TYPE, ELEMENT;
    public:
        explicit GearItem(bool isStandardItem, std::string name, std::string desc, num value, num geartype, num effecttype, num elem, num item_value);
        virtual ~GearItem();

        num getValue() const;
        num getGearType() const;
        num getEffectType() const;
        num getElement() const;
    };

}

#endif // GEARITEM_H
