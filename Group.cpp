#include "Group.h"
#include "Player.h"

using namespace grandia;

/**
 * @brief Group::Group Constructor. Creates a bag on the heap
 */
Group::Group() {
    _itembag = new Bag;
}

/**
 * @brief Group::~Group Destructor. Deletes the itembag
 */
Group::~Group() {
    delete _itembag;
    _itembag = nullptr;
}

/**
 * @brief Group::setMoney add or remove money from the playergroup
 * @param money amount of money
 */
void Group::setMoney(short money){
    if ( _money + money < 0 ){
        _money = 0;
    } else {
        _money += money;
    }
}

/**
 * @brief Group::setXp set temporaly XP value what each player gets after the fight
 * @param xp xp value
 */
void Group::setXp(num xp){
    _xp += xp;
}

/**
 * @brief Group::resetXp sets the xp value to 0
 */
void Group::resetXp(){
    _xp = 0;
}

/**
 * @brief Group::addMember add a member into the group
 * @param actor actor must be a player not an enemy
 */
void Group::addMember(Role *player){
    _members.push_back(player);
    dynamic_cast<Player*>(player)->setPlayersGroup(this);
}

/**
 * @brief Group::addItem add a item in the itembag
 * @param item item
 */
void Group::addItem(Item *item){
    _itembag->addItem(item);
}

/**
 * @brief Group::addItemsFromFight Add the dropitemvector from the enemy after a successful fight
 * @param items itemvector
 */
void Group::addItemsFromFight(std::vector<Item *> *items){
    for(Item *item : *items){
        _itembag->addItem(item);
    }
}

/**
 * @brief Group::getMoney returns the amount of groupmoney. Each player has access to it
 * @return groupmoney
 */
num Group::getMoney() const {
    return _money;
}

/**
 * @brief Group::getXpFromFight returns the temporaly XP value from the fight result. After this the
 * resetXP function should be called
 * @return amount of xp from the fight
 */
num Group::getXpFromFight() const {
    return _xp;
}

/**
 * @brief Group::getMember returns a groupmember
 * @param idx index of the player
 * @return groupmember
 */
Role* Group::getMember(num idx) {
    return _members.at(idx);
}

/**
 * @brief Group::getItem returns a selected item from the itembag
 * @param idx index of the item
 * @return item
 */
Item* Group::getItem(num idx){
    return _itembag->getItem(idx);
}

/**
 * @brief Group::getItemBag returns the itembag
 * @return itembag
 */
Bag* Group::getItemBag() {
    return _itembag;
}

/**
 * @brief Group::getGroupMembers returns all players from the group
 * @return vector of players
 */
std::vector<Role*>* Group::getGroupMembers(){
    return &_members;
}
