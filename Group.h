#ifndef GROUP_H
#define GROUP_H

#include <vector>
#include "Role.h"

namespace grandia {

class Group {
    std::vector<Role*> _members;
    num _money;
    num _xp;
    Bag *_itembag;
public:
    explicit Group();
    virtual ~Group();

    void setMoney(short money);
    void setXp(num xp);
    void resetXp();
    void addMember(Role *actor);
    void addItem(Item *item);
    void addItemsFromFight(std::vector<Item*>* items);

    num getMoney() const;
    num getXpFromFight() const;
    Role* getMember(num idx);
    Item* getItem(num idx);
    Bag* getItemBag();
    std::vector<Role*>* getGroupMembers();
};

}

#endif // GROUP_H
