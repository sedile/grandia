#include <iostream>
#include <thread>
#include <chrono>

#include "IPGauge.h"
#include "Code.h"
#include "StandardItem.h"
#include "Player.h"


using namespace grandia;

/**
 * @brief IPGauge::IPGauge The IP-Gauge
 * @param actors list of all actors in the fight
 * @param group group which contains players only
 */
IPGauge::IPGauge(std::vector<Role*> &actors, Group *group) : _actors(actors), _fight(actors, group) { }

/**
 * @brief IPGauge::~IPGauge Destructor
 */
IPGauge::~IPGauge() { }

/**
 * @brief IPGauge::progress The whole gameprocess. This function simulated the IP-Gauge
 */
void IPGauge::progress(){
    for(num i = 0; i < _actors.size(); i++){
        if ( _actors.at(i)->getActHP() > 0 ){
            examineStatusEffectsOnIPGauge(i);
            examineStatusEffectsOnActor(i);
        }

        std::this_thread::sleep_for(std::chrono::milliseconds(100));
        //std::cout << _actors.at(i)->getIP() << "\n";

        /* An Actor reaches COM */
        if ( _actors.at(i)->getIP() >= COM && !_actors.at(i)->getCastPhraseFlag() ){
            _actors.at(i)->updateActorsAction("Command");
            _actors.at(i)->setIP(-(_actors.at(i)->getIP() - COM) + 1);

            char cmd = ErrorCode::ERROR_CODE_INPUT;
            num target = ErrorCode::ERROR_CODE_TARGET;
            do {
                _fight.printMessage(_actors.at(i)->getName() + " COMMAND INPUT");
                setChoose(cmd, target, i);
            } while ( cmd == ErrorCode::ERROR_CODE_INPUT || target == ErrorCode::ERROR_CODE_TARGET );

            _actors.at(i)->setTarget(target);
            _actors.at(i)->setCastPhraseFlag(true);
            _actors.at(i)->setPreparedAction(cmd);
            _actors.at(i)->setDefenseMode(false);

            if ( cmd == MenuCode::COMBO || cmd == MenuCode::CRITICAL || cmd == MenuCode::DEFEND || cmd == MenuCode::EVADE ){
               immediately(i);
            } else {
                slowdown(i);
            }

        } else if ( _actors.at(i)->getIP() >= ACT ){
            _fight.printMessage(_actors.at(i)->getName() + " ACT");

            /* If the target died in the meanwhile, choose another one */
            num target = _actors.at(i)->getTarget();
            if ( _actors.at(target)->getActHP() == 0 ){
                _fight.printMessage(_actors.at(target)->getName() + " is dead. Another target was choosed.");
                for(num n = 0; n < _actors.size(); n++){
                    if ( _actors.at(target)->isPlayer() == _actors.at(n)->isPlayer() && _actors.at(n)->getActHP() > 0){
                        _actors.at(i)->setTarget(n);
                        break;
                    }
                }
            }

            /* An Actor reaches ACT. After that, begin from zero and reach COM */
            _fight.commandAction(_actors.at(i)->getActionType(), i, _actors.at(i)->getTarget());
            _actors.at(i)->resetIP();
            _actors.at(i)->setCastPhraseFlag(false);
            _actors.at(i)->updateActorsAction("Wait");
        }
    }
}

/**
 * @brief IPGauge::setChoose Choose the target and the type of an action like magic, special, info, ...
 * @param cmd the choosen command
 * @param target the choosen target
 * @param i the Actor who choose the command and target
 */
void IPGauge::setChoose(char &cmd, num &target, num &i){
    cmd = _fight.commandInput(i);
    if ( cmd == MenuCode::COMBO || cmd == MenuCode::CRITICAL || cmd == MenuCode::SPECIAL ) {
        target = _fight.chooseTarget(i);
    } else if ( cmd == MenuCode::MAGIC && _actors.at(i)->getChoosedMagic()->getType() == AttackCode::ATTACK ){
        target = _fight.chooseTarget(i);
    } else if ( cmd == MenuCode::MAGIC && _actors.at(i)->getChoosedMagic()->getType() == AttackCode::HEAL ){
        target = _fight.chooseGroupTarget(_actors.at(i)->isPlayer());
    } else if ( cmd == MenuCode::MAGIC && _actors.at(i)->getChoosedMagic()->getType() == AttackCode::BOOST ){
        target = _fight.chooseGroupTarget(_actors.at(i)->isPlayer());
    } else if ( cmd == MenuCode::MAGIC && _actors.at(i)->getChoosedMagic()->getType() == AttackCode::DECREASE ){
        target = _fight.chooseTarget(i);
    } else if ( cmd == MenuCode::MAGIC && _actors.at(i)->getChoosedMagic()->getType() == SpecialCode::RESURRECT){
        target = _fight.chooseGroupTarget(_actors.at(i)->isPlayer());
    } else if ( cmd == MenuCode::MAGIC && _actors.at(i)->getChoosedMagic()->getType() == SpecialCode::HALVAH){
        target = _fight.chooseGroupTarget(_actors.at(i)->isPlayer());

    /* Vanish is an enemy only heal spell */
    } else if ( !_actors.at(i)->isPlayer() && cmd == MenuCode::MAGIC && _actors.at(i)->getChoosedMagic()->getType() == SpecialCode::VANISH){
        target = _fight.chooseGroupTarget(_actors.at(i)->isPlayer());
    } else if ( cmd == MenuCode::ITEM && dynamic_cast<StandardItem*>(_actors.at(i)->getChoosedItem())->getType() == AttackCode::ATTACK ){
        target = _fight.chooseTarget(i);
    } else if ( cmd == MenuCode::ITEM && dynamic_cast<StandardItem*>(_actors.at(i)->getChoosedItem())->getType() == AttackCode::HEAL ){
        target = _fight.chooseGroupTarget(_actors.at(i)->isPlayer());
    } else if ( cmd == MenuCode::ITEM && dynamic_cast<StandardItem*>(_actors.at(i)->getChoosedItem())->getType() == AttackCode::BOOST ){
        target = _fight.chooseGroupTarget(_actors.at(i)->isPlayer());
    } else if ( cmd == MenuCode::ITEM && dynamic_cast<StandardItem*>(_actors.at(i)->getChoosedItem())->getType() == AttackCode::DECREASE ){
        target = _fight.chooseTarget(i);
    } else if ( cmd == MenuCode::DEFEND || cmd == MenuCode::EVADE ){
        target = i;
    } else if ( cmd == MenuCode::INFO ){
        _fight.showInfo();
        cmd = ErrorCode::ERROR_CODE_INPUT;
    }
}

/**
 * @brief IPGauge::examineStatusEffectsOnIPGauge Effects Status-Ailments on the IP-Gauge (Stop, Paralyze)
 * @param i The Actors ID
 */
void IPGauge::examineStatusEffectsOnIPGauge(num &i){
    Role *actor = _actors.at(i);
    if ( actor->getStatus()->getParalyzeFlag() ){
        actor->getStatus()->updateParalyzeTimer(actor->getSpeed());
        actor->resetIP();
        actor->setCastPhraseFlag(false);
        actor->updateActorsAction("Wait");

        if ( actor->isPlayer() && actor->getChoosedItem() != nullptr ){
            Player *player = dynamic_cast<Player*>(actor);
            player->getPlayersGroup()->getItemBag()->addItem(actor->getChoosedItem());
            player->chooseItem(nullptr);
        }

        if ( actor->getStatus()->getParalyzeTimer() == 0 ){
             actor->getStatus()->setParalyzeFlag(false);
             actor->getStatus()->resetParalyzeTimer(); 
        }
    } else if ( actor->getStatus()->getStopFlag() ){
        actor->setIP(0);
        actor->getStatus()->updateStopTimer(actor->getSpeed());
        if ( actor->getStatus()->getStopTimer() == 0 ){
            actor->getStatus()->setStopFlag(false);
            actor->getStatus()->resetStopTimer();    
        }
    } else if ( actor->getStatus()->getGigaVanishFlag() ){
        actor->getStatus()->updateGigaVanishTimer(actor->getSpeed());
        actor->getStatus()->gigavanish();
        if ( actor->getStatus()->getGigaVanishTimer() == 0 ){
            actor->getStatus()->setGigaVanishFlag(false);
            actor->getStatus()->resetGigaVanishTimer();    
        }
    } else {
        actor->setIP(actor->getSpeed());
    }
    actor = nullptr;
}

/**
 * @brief IPGauge::immediately sets the IP value to ACT for combo,critical,defense and evade
 * @param caster the caster
 */
void IPGauge::immediately(num &caster){
    _actors.at(caster)->setIP(ACT - COM);
}

/**
 * @brief IPGauge::slowdown slows down the speed of IP progress if the actor uses magic,move or items
 * @param caster the caster
 */
void IPGauge::slowdown(num &caster){
    if ( _actors.at(caster)->getChoosedMagic() != nullptr ){
        _actors.at(caster)->setIP(_actors.at(caster)->getSpeed() - _actors.at(caster)->getChoosedMagic()->getEffectLevel());
    } else if ( _actors.at(caster)->getChoosedMove() != nullptr ){
        _actors.at(caster)->setIP(_actors.at(caster)->getSpeed() - _actors.at(caster)->getChoosedMove()->getEffectLevel());
    } else if ( _actors.at(caster)->isPlayer() && _actors.at(caster)->getChoosedItem() != nullptr ){
        dynamic_cast<Player*>(_actors.at(caster))->setIP(_actors.at(caster)->getSpeed() - 5);
    }
}

/**
 * @brief IPGauge::examineStatusEffectsOnActor Effects Status-Ailments on the Actor
 * @param i The Actors ID
 */
void IPGauge::examineStatusEffectsOnActor(num &i){
    Role *actor = _actors.at(i);
    if ( actor->getStatus()->getPoisonFlag() && actor->getIP() % 400 == 0){
        num poisondamage = actor->getMaxHP() * 0.05;
        actor->setHP(-poisondamage);
        _fight.printMessage(actor->getName() + " takes " + std::to_string(poisondamage) + " Poison Damage");
    } else if ( actor->getStatus()->getIllnesFlag() && actor->getIP() % 400 == 0){
        num rand_ailment = std::rand();
        switch( rand_ailment % 7 ){
        case 1 : {
            actor->getStatus()->setPoisonFlag(true);
            break;
        }
        case 2 : {
            actor->getStatus()->setParalyzeFlag(true);
            actor->getStatus()->updateParalyzeTimer(actor->getSpeed());
            break;
        }
        case 3 : {
            actor->getStatus()->setStopFlag(true);
            actor->getStatus()->updateStopTimer(actor->getSpeed());
            break;
        }
        case 4 : {
            actor->getStatus()->setMagicBlockFlag(true);
            break;
        }
        case 5 : {
            actor->getStatus()->setMoveBlockFlag(true);
            break;
        }
        case 6 : {
            actor->getStatus()->setCurseFlag(true);
            break;
        }
        case 7 : {
            actor->getStatus()->setGigaVanishFlag(true);
            actor->getStatus()->updateGigaVanishTimer(actor->getSpeed());
            break;
        }
        default : { }
        }
    }
    actor = nullptr;
}

/**
 * @brief IPGauge::getActorIP Returns the actual IP-Value of an Actor
 * @param idx The ID of the Actor
 * @return the IP-Value of the Actor
 */
num IPGauge::getActorIP(num &idx) const {
    return _actors.at(idx)->getIP();
}

/**
 * @brief IPGauge::getFightStatus returns the fightstatus
 * @return fightstatus,
 * 0 = continious, 1 = player wins, 2 = enemy wins
 */
num IPGauge::getFightStatus() const {
    return _fight.getFightStatus();
}
