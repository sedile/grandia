#ifndef __IPGAUGE_H__
#define __IPGAUGE_H__

#include <vector>
#include "Fight.h"
#include "Group.h"

namespace grandia {

    typedef unsigned short num;

    class IPGauge {
    private:
        std::vector<Role*> _actors;
        Fight _fight;
        void examineStatusEffectsOnIPGauge(num &idx);
        void examineStatusEffectsOnActor(num &idx);
        void setChoose(char &cmd, num &target, num &idx);
        void immediately(num &caster);
        void slowdown(num &caster);
    public:
        static const num WAIT = 0;
        static const num COM = 1000;
        static const num ACT = 1500;

        explicit IPGauge(std::vector<Role*> &actors, Group *group);
        virtual ~IPGauge();

        void progress();

        num getActorIP(num &idx) const;
        num getFightStatus() const;
    };

}

#endif
