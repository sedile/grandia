#include "Item.h"

using namespace grandia;

/**
 * @brief Item::Item The baseclass for Items
 * @param isStandardItem true, if this is a StandardItem which can be used in the fight. False
 * if this is gearitem.
 * @param name Name of this item
 * @param desc Description of this item. Should not be empty
 * @param item_value value of this item for buy and sell
 */
Item::Item(bool isStandardItem, std::string name, std::string desc, num item_value)
: IS_STANDARD_ITEM(isStandardItem), NAME(name), DESCRIPTION(desc), ITEM_VALUE(item_value) { }

/**
 * @brief Item::~Item Destructor
 */
Item::~Item() { }

/**
 * @brief Item::isStandardItem returns the Itemtype
 * @return true, Standarditem, false, Gearitem
 */
bool Item::isStandardItem() const {
    return IS_STANDARD_ITEM;
}

/**
 * @brief Item::getName returns the name of this item
 * @return itemname
 */
std::string Item::getName() const {
    return NAME;
}

/**
 * @brief Item::getDescription returns the description of this item
 * @return description
 */
std::string Item::getDescription() const {
    return DESCRIPTION;
}

/**
 * @brief Item::getItemValue returns the itemvalue for vendors
 * @return value of this item
 */
num Item::getItemValue() const {
    return ITEM_VALUE;
}
