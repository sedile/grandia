#ifndef __ITEM_H__
#define __ITEM_H__

#include <string>
#include <vector>

namespace grandia {

    typedef unsigned short num;

    class Item {
    private:
        const bool IS_STANDARD_ITEM;
        const std::string NAME, DESCRIPTION;
        const float ITEM_VALUE;
    public:
        explicit Item(bool isStandardItem, std::string name, std::string desc, num item_value);
        virtual ~Item();

        virtual bool isStandardItem() const;
        virtual std::string getName() const;
        virtual std::string getDescription() const;
        virtual num getItemValue() const;
    };

}

#endif
