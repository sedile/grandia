#include "Magic.h"

using namespace grandia;

/**
 * @brief Magic::Magic The Magicspell class
 * @param name Name of this spell
 * @param description description of this spell
 * @param mpu how many MP this spell uses
 * @param type type of this spell (Attack,Heal,Buff,Decrease)
 * @param distance distance of this spell (Singe,Radius,All)
 * @param statusflag what statuseffect this spell can do
 * @param elem element of this spell (Fire,Wind,Water,Earth)
 * @param el effectlevel, the own strength of this spell
 */
Magic::Magic(std::string name, std::string description, num mpu, num type, num distance, num statusflag, num elem, num el)
: NAME(name), DESCRIPTION(description), MP_USAGE(mpu), TYPE(type), EFFECT_LEVEL(el), DISTANCE(distance), STATUS_FLAG(statusflag), ELEMENT(elem) { }

/**
 * @brief Magic::~Magic Destructor
 */
Magic::~Magic() { }

/**
 * @brief Magic::getName returns the name of this spell
 * @return spellname
 */
std::string Magic::getName() const {
    return NAME;
}

/**
 * @brief Magic::getDescription returns the description of this spell
 * @return description
 */
std::string Magic::getDescription() const {
    return DESCRIPTION;
}

/**
 * @brief Magic::getMPUse returns the MP used by this spell
 * @return m usage
 */
num Magic::getMPUse() const {
    return MP_USAGE;
}

/**
 * @brief Magic::getType returns the type of this spell
 * @return type (attack,heal,buff,decrease)
 */
num Magic::getType() const {
    return TYPE;
}

/**
 * @brief Magic::getElement returns the element of this spell
 * @return element
 */
num Magic::getElement() const {
    return ELEMENT;
}

/**
 * @brief Magic::getDistance returns the distance what this spell can reach
 * @return distance (single,radius,all)
 */
num Magic::getDistance() const {
    return DISTANCE;
}

/**
 * @brief Magic::getEffectLevel returns the own strength of this spell
 * @return effectlevel
 */
num Magic::getEffectLevel() const {
    return EFFECT_LEVEL;
}

/**
 * @brief Magic::getStatusFlag returns what statuseffect this spell can do
 * @return statuseffect
 */
num Magic::getStatusFlag() const {
    return STATUS_FLAG;
}
