#ifndef __MAGIC_H__
#define __MAGIC_H__

#include <string>

namespace grandia {

    typedef unsigned short num;

    class Magic {
    private:
        const std::string NAME, DESCRIPTION;
        const num MP_USAGE, TYPE, EFFECT_LEVEL, DISTANCE, STATUS_FLAG, ELEMENT;
    public:
        explicit Magic(std::string name, std::string description, num mpu, num type, num distance, num statusflag, num elem, num effectlevel);
        virtual ~Magic();

        std::string getName() const;
        std::string getDescription() const;
        num getMPUse() const;
        num getType() const;
        num getElement() const;
        num getDistance() const;
        num getEffectLevel() const;
        num getStatusFlag() const;
        num getNumberOfHits() const;
    };

}

#endif
