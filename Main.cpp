#include "Player.h"
#include "Enemy.h"
#include "Boss.h"
#include "IPGauge.h"
#include "Magic.h"
#include "SpellContainer.h"
#include "Move.h"
#include "MoveContainer.h"
#include "Bag.h"
#include "Code.h"
#include "Item.h"
#include "StandardItem.h"
#include "GearItem.h"
#include "Group.h"
#include "Vendor.h"

#include <vector>
#include <iostream>

using namespace grandia;

int main(){

    // TODO : Save/Load predefined magic,move,enemy,boss and player data persistent (SQL?)

    // Test : Enemy drops this Item after victory
    Item *drop = new StandardItem(true,"Herb","Heals 20 HP",20,ElemCode::WATER,TargetCode::SINGLE_TARGET,AttackCode::HEAL,StatusCode::NO_STATUS, 20);

    // Create some players and enemies
    Role *player = new Player("Spieler",180,80,110,1,0,999,60,30,95,0);
    Role *player2 = new Player("Spielerbegleiter",70,55,80,1,0,999,50,35,65,0);
    Role *enemy = new Enemy("Gegner",30,130,150,1,58,100,50,20,80,0);
    Role *enemy2 = new Enemy("Gegnerbegleiter",60,130,150,4,66,75,60,50,50,0);
    //Role *boss = new Boss("Hydra (Main)",true,50,0,0,1,50,50,5,5,5,5);
    //Role *boss2 = new Boss("Hydra - Heathead",false,700,5,5,5,5,5,5,5,5,5);

    // Add players to group. Group exists after Fight
    Group *group = new Group;
    group->addMember(player);
    group->addMember(player2);

    // Add testitem to enemy
    dynamic_cast<Enemy*>(enemy)->addDropItemToList(drop);
    dynamic_cast<Enemy*>(enemy2)->addDropItemToList(drop);

    // Add some Items into the groups item bag (every member can access this group item bag)
    group->addItem(new StandardItem(true, "Heiltrank", "Heilt 200 HP", 200, ElemCode::WATER, TargetCode::SINGLE_TARGET, AttackCode::HEAL,StatusCode::NO_STATUS, 25));
    group->addItem(new StandardItem(true, "Feuerrolle", "Sprich ein Feuerzauber auf mehrere Ziele", 105, ElemCode::FIRE, TargetCode::RADIUS_TARGET, AttackCode::ATTACK, StatusCode::NO_STATUS, 30));
    group->addItem(new StandardItem(true, "Ausbruch", "Erhoeht den Angriff voruebergehend", 1, ElemCode::FIRE, TargetCode::SINGLE_TARGET, AttackCode::BOOST, StatusCode::ATK_UP, 30));
    group->addItem(new StandardItem(true, "Heilstein", "Heilt alle um 100 HP", 100, ElemCode::WATER, TargetCode::ALL_TARGET, AttackCode::HEAL, StatusCode::NO_STATUS, 40));
    group->addItem(new GearItem(false, "Schwert","Ein Schwert", 4,GearCode::WEAPON,StatusCode::NO_STATUS,ElemCode::PHYSICAL, 500));

    // Create some magic
    std::vector<Magic*> magiclist;
    magiclist.push_back(new Magic("Burnflame", "", 10, AttackCode::ATTACK, TargetCode::RADIUS_TARGET, StatusCode::NO_STATUS, ElemCode::FIRE, 5));
    magiclist.push_back(new Magic("Howl", "", 4, AttackCode::ATTACK, TargetCode::SINGLE_TARGET, StatusCode::NO_STATUS, ElemCode::WIND, 2));
    magiclist.push_back(new Magic("Hellburner", "", 4, AttackCode::ATTACK, TargetCode::SINGLE_TARGET, StatusCode::NO_STATUS, ElemCode::FIRE, 5));
    magiclist.push_back(new Magic("Resurrect", "", 13, SpecialCode::RESURRECT, TargetCode::SINGLE_TARGET, StatusCode::NO_STATUS, ElemCode::WATER, 1));
    magiclist.push_back(new Magic("Alheal", "", 18, AttackCode::HEAL, TargetCode::ALL_TARGET, StatusCode::NO_STATUS, ElemCode::PHYSICAL, 8));
    magiclist.push_back(new Magic("Vanish","",3,SpecialCode::VANISH,TargetCode::ALL_TARGET,StatusCode::NO_STATUS, ElemCode::WATER, 5));
    SpellContainer *testmagic = new SpellContainer(magiclist);

    // Create some moves
    std::vector<Move*> movelist;
    movelist.push_back(new Move("X-Slash", "", 14, AttackCode::ATTACK, TargetCode::SINGLE_TARGET, ElemCode::PHYSICAL, 6));
    movelist.push_back(new Move("Spark Volt", "", 24, AttackCode::ATTACK, TargetCode::RADIUS_TARGET, ElemCode::WIND, 9));
    movelist.push_back(new Move("Divine Dragon Slash", "", 72, AttackCode::ATTACK, TargetCode::ALL_TARGET, ElemCode::PHYSICAL, 30));
    MoveContainer *testmoves = new MoveContainer(movelist);

    // give players and enemies positions, moves and magic
    player->setInitActorPosition(10,10);
    player2->setInitActorPosition(30,10);
    enemy->setInitActorPosition(60,10);
    enemy2->setInitActorPosition(61,10);
    player->setSpellContainer(testmagic);
    player2->setSpellContainer(testmagic);
    enemy->setSpellContainer(testmagic);
    enemy2->setSpellContainer(testmagic);
    player->setMoveContainer(testmoves);
    player2->setMoveContainer(testmoves);
    enemy->setMoveContainer(testmoves);
    enemy2->setMoveContainer(testmoves);

    // Give all some resistence against elements
    player->initResistence(1,1,1,1);
    player2->initResistence(1,1,1,1);
    enemy->initResistence(1,1,1,1);
    enemy2->initResistence(1,1,1,1);

    // Needed for IP-Gauge and Fight
    std::vector<Role*> actors;
    actors.push_back(player);
    actors.push_back(player2);
    actors.push_back(enemy);
    actors.push_back(enemy2);

    IPGauge ip(actors, group);

    // Game loop
    while(ip.getFightStatus() == 0){
        ip.progress();
    }

    // Fight results
    if ( ip.getFightStatus() == 1 ){
        std::cout << "Playergroup loose" << "\n";
    } else if ( ip.getFightStatus() == 2 ){
        std::cout << "Enemygroup loose" << "\n";

        for(num i = 0; i < group->getGroupMembers()->size(); i++){
            Player *member = dynamic_cast<Player*>(group->getMember(i));
            member->setXP(group->getXpFromFight());
            std::cout << member->getName() << " gets " << group->getXpFromFight() << " XP\n";
            std::cout << member->getXp() << " / " << member->getXpToNextLevel() << "\n\n";
        }
        group->resetXp();
        std::cout << "Group has " << group->getMoney() << " Money\n";

        // Print Itembag after victory (Test) //
        group->getItemBag()->printItemList();
    }

    std::vector<Item*> items;
    items.push_back(new StandardItem(true, "Heiltrank", "Heilt 200 HP", 200, ElemCode::WATER, TargetCode::SINGLE_TARGET, AttackCode::HEAL,StatusCode::NO_STATUS, 25));
    items.push_back(new StandardItem(true, "Feuerrolle", "Sprich ein Feuerzauber auf mehrere Ziele", 105, ElemCode::FIRE, TargetCode::RADIUS_TARGET, AttackCode::ATTACK, StatusCode::NO_STATUS, 30));
    items.push_back(new GearItem(false, "Eisenschwert","Ein Schwert aus Eisen", 4,GearCode::WEAPON,StatusCode::NO_STATUS,ElemCode::PHYSICAL, 500));
    Vendor v("Karl",items);
    v.buyItem(group);
    v.sellItem(group);
    v.printItemList();
    group->getItemBag()->printItemList();

    return 0;
}
