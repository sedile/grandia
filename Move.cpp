#include "Move.h"

using namespace grandia;

/**
 * @brief Magic::Magic The Movespell class
 * @param name Name of this move
 * @param description description of this move
 * @param spu how many SP this move uses
 * @param type type of this move (Attack,Heal,Buff,Decrease)
 * @param distance distance of this move (Singe,Radius,All)
 * @param statusflag what statuseffect this move can do
 * @param elem element of this move (None,Fire,Wind,Water,Earth)
 * @param el effectlevel, the own strength of this move
 */
Move::Move(std::string name, std::string description, num spu, num type, num distance, num elem, num el)
: NAME(name), DESCRIPTION(description), SP_USAGE(spu), TYPE(type), DISTANCE(distance), ELEMENT(elem), EFFECT_LEVEL(el) { }

/**
 * @brief Move::~Move Destructor
 */
Move::~Move() { }

/**
 * @brief Move::getName returns the name of this move
 * @return name of the move
 */
std::string Move::getName() const {
    return NAME;
}

/**
 * @brief Move::getDescription returns the description of this move
 * @return description
 */
std::string Move::getDescription() const {
    return DESCRIPTION;
}

/**
 * @brief Move::getSPUse returns the amount of SP what this move uses
 * @return sp usage
 */
num Move::getSPUse() const {
    return SP_USAGE;
}

/**
 * @brief Move::getType returns the type fo this move
 * @return type (attack,heal,boost,descreae)
 */
num Move::getType() const {
    return TYPE;
}

/**
 * @brief Move::getElement returns the elementtype of this move
 * @return elementtype
 */
num Move::getElement() const {
    return ELEMENT;
}

/**
 * @brief Move::getDistance returns the distance what this move can reaches
 * @return distance (single,radius,all)
 */
num Move::getDistance() const {
    return DISTANCE;
}

/**
 * @brief Move::getEffectLevel returns the own strength of this move
 * @return effectlevel
 */
num Move::getEffectLevel() const {
    return EFFECT_LEVEL;
}
