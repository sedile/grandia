#ifndef __MOVE_H__
#define __MOVE_H__

#include <string>

namespace grandia {

    typedef unsigned short num;

    class Move {
    private:
        const std::string NAME, DESCRIPTION;
        const num SP_USAGE, TYPE, DISTANCE, ELEMENT, EFFECT_LEVEL;
    public:
        explicit Move(std::string name, std::string description, num spu, num type, num distance, num elem, num effectlevel);
        virtual ~Move();

        std::string getName() const;
        std::string getDescription() const;
        num getSPUse() const;
        num getType() const;
        num getElement() const;
        num getDistance() const;
        num getEffectLevel() const;
        num getNumberOfHits() const;
    };

}

#endif
