#include "MoveContainer.h"
#include <iostream>

using namespace grandia;

/**
 * @brief MoveContainer::MoveContainer Containerclass for moves
 * @param movelist vector of moves
 */
MoveContainer::MoveContainer(std::vector<Move*> &movelist) {
    if ( !movelist.empty() && movelist.size() <= 10 ){
        _movelist = movelist;
    } else {
        std::cout << "Error at MoveContainer Init" << "\n";
    }
}

/**
 * @brief MoveContainer::~MoveContainer Destructor. Deletes all moves from this list
 */
MoveContainer::~MoveContainer() { 
    for(num i = 0; i < _movelist.size(); i++){
        delete _movelist.at(i);
    }
}

/**
 * @brief MoveContainer::printMoveList prints informations about all moves
 */
void MoveContainer::printMoveList() {
    for(num i = 0; i < _movelist.size(); i++){
        std::cout << "[" << std::to_string(i) << "] ";
        std::cout << _movelist.at(i)->getName() << " | ";
        std::cout << _movelist.at(i)->getDescription() << " | ";
        std::cout << std::to_string(_movelist.at(i)->getSPUse()) << " | ";
        std::cout << _movelist.at(i)->getType() << " | ";
        std::cout << std::to_string(_movelist.at(i)->getEffectLevel()) << "\n";
    }
}

/**
 * @brief MoveContainer::getMove get one move from the list
 * @param idx index of this move
 * @return move
 */
Move* MoveContainer::getMove(num &idx) const {
    if ( idx > _movelist.size() ){
        return nullptr;
    }
    return _movelist.at(idx);
}

/**
 * @brief MoveContainer::getSize returns the size of the container
 * @return size
 */
num MoveContainer::getSize() const {
    return _movelist.size();
}
