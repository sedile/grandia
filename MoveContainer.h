#ifndef __MOVECONTAINER_H__
#define __MOVECONTAINER_H__

#include <string>
#include <vector>

#include "Move.h"

namespace grandia {

    class MoveContainer {
    private:
        std::vector<Move*> _movelist;
    public:
        explicit MoveContainer(std::vector<Move*> &movelist);
        virtual ~MoveContainer();

        void printMoveList();
        Move* getMove(num &idx) const;
        num getSize() const;
    };

}

#endif
