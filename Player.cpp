#include "Player.h"
#include "Code.h"

using namespace grandia;

/**
 * @brief Player::Player Playerclass
 * @param name Name of the player
 * @param isPlayer must be true
 * @param mhp Maximum HP
 * @param mmp Maximum MP
 * @param msp Maximum SP
 * @param level Level of this player
 * @param actXP actual xp from this player
 * @param xpToLevelUp xp needes to level up
 * @param str strength (Damage)
 * @param def defense (Damage reduction)
 * @param spd Speed (speed on IP-Gauge)
 * @param mov Movement (movespeed in battlefield)
 */
Player::Player(std::string name, num mhp, num mmp, num msp, num level, num actXP, num xpToLevelUp, num str, num def, num spd, num mov)
    : Role(name,true,mhp,mmp,msp,level,str,def,spd,mov), _actXp(actXP), _xpToLevelUp(xpToLevelUp), _group(nullptr) { }

/**
 * @brief Player::~Player Destructor
 */
Player::~Player() { }

void Player::setPlayersGroup(Group *group){
    _group = group;
}

/**
 * @brief Player::addWeaponXp adds xp to weapon
 * @param xp amount of xp
 */
void Player::addWeaponXp(num &xp){
    _weaponXp += xp;
    if ( _weaponXp > 100 && _weaponLv < 99 ){
        setWeaponLevel(getWeaponLevel() + 1);
        // TODO : Increase one Parameter permanently
        if ( getWeaponLevel() >= 99 ){
            _weaponXp = 99;
        } else {
            _weaponXp -= 100;
        }
    } else {
        _weaponXp = 99;
     }
}

/**
 * @brief Player::addFireXp adds xp to fire
 * @param xp amount of xp
 */
void Player::addFireXp(num &xp){
    _fireXp += xp;
    if ( _fireXp > 100 && _fireLv < 99 ){
        setFireLevel(getFireLevel() + 1);
        // TODO : Learn Magic and Moves by Specific Level (Burnflame : FireLv = 4, W-Slash : WeaponLv = 5)
        // TODO : Increase one Parameter permanently
        if ( getFireLevel() >= 99 ){
            _fireXp = 99;
        } else {
            _fireXp -= 100;
        }
    } else {
        _fireXp = 99;
     }
}

/**
 * @brief Player::addWindXp adds xp to wind
 * @param xp amount of xp
 */
void Player::addWindXp(num &xp){
    _windXp += xp;
    if ( _windXp > 100 && _windLv < 99 ){
        setWindLevel(getWindLevel() + 1);
        // TODO : Increase one Parameter permanently
        if ( getWindLevel() >= 99 ){
            _windXp = 99;
        } else {
            _windXp -= 100;
        }
    } else {
        _windXp = 99;
     }
}

/**
 * @brief Player::addWaterXp adds xp to water
 * @param xp amount of xp
 */
void Player::addWaterXp(num &xp){
    _waterXp += xp;
    if ( _waterXp > 100 && _waterLv < 99 ){
        setWaterLevel(getWaterLevel() + 1);
        // TODO : Increase one Parameter permanently
        if ( getWaterLevel() >= 99 ){
            _waterXp = 99;
        } else {
            _waterXp -= 100;
        }
    } else {
        _waterXp = 99;
     }
}

/**
 * @brief Player::addEarthXp adds xp to earth
 * @param xp amount of xp
 */
void Player::addEarthXp(num &xp){
    _earthXp += xp;
    if ( _earthXp > 100 && _earthLv < 99 ){
        setEarthLevel(getEarthLevel() + 1);
        // TODO : Increase one Parameter permanently
        if ( getEarthLevel() >= 99 ){
            _earthXp = 99;
        } else {
            _earthXp -= 100;
        }
    } else {
       _earthXp = 99;
    }
}

/**
 * @brief Player::setWeaponLevel sets the weapon level
 * @param lv the level
 */
void Player::setWeaponLevel(num lv){
    _weaponLv = lv;
}

/**
 * @brief Player::setFireLevel sets the fire level
 * @param lv the level
 */
void Player::setFireLevel(num lv){
    _fireLv = lv;
}

/**
 * @brief Player::setWindLevel sets the wind level
 * @param lv the level
 */
void Player::setWindLevel(num lv){
    _windLv = lv;
}

/**
 * @brief Player::setWaterLevel sets the waterlevel
 * @param lv the level
 */
void Player::setWaterLevel(num lv){
    _waterLv = lv;
}

/**
 * @brief Player::setEarthLevel sets the earthlevel
 * @param lv the level
 */
void Player::setEarthLevel(num lv){
    _earthLv = lv;
}

/**
 * @brief Player::setXP sets the xp
 * @param exp xp points
 */
void Player::setXP(num exp){
    if ( _actXp + exp >= _xpToLevelUp ){
        setLevel(getLevel() + 1);
        _actXp = _actXp + exp - _xpToLevelUp;
    } else {
        _actXp += exp;
    }
}

/**
 * @brief Player::setXpToLevelUp set the value what xp is needed to level up
 * @param xp new xp bound for new level
 */
void Player::setXpToLevelUp(num xp){
    _xpToLevelUp = xp;
}

/**
 * @brief Player::equipGear Equips the player witch Gearitems
 * @param gearitem the gearitem
 * @param piece what kind of gearitem
 * @param remove true, if the actual gearitem should be replaced
 */
void Player::equipGear(GearItem *gearitem, num piece, bool remove){
    if ( remove ){
        switch(piece) {
        case GearCode::WEAPON : {
            _gear.getWeapon(); // TODO : Insert into groups item bag
            break;
        }
        case GearCode::HEAD_ARMOR : {
            _gear.getHeadArmor();
            break;
        }
        case GearCode::HAND_ARMOR : {
            _gear.getHandArmor();
            break;
        }
        case GearCode::CHEST_ARMOR : {
            _gear.getChestArmor();
            break;
        }
        case GearCode::FOOT_ARMOR : {
            _gear.getFootArmor();
            break;
        }
        case GearCode::ACCESSOIRE : {
            _gear.getAccessoire();
            break;
        }
        default : { }
        }
    } else {
        switch(piece) {
        case GearCode::WEAPON : {
            _gear.addWeapon(gearitem);
            break;
        }
        case GearCode::HEAD_ARMOR : {
            _gear.addHeadArmor(gearitem);
            break;
        }
        case GearCode::HAND_ARMOR : {
            _gear.addHandArmor(gearitem);
            break;
        }
        case GearCode::CHEST_ARMOR : {
            _gear.addChestArmor(gearitem);
            break;
        }
        case GearCode::FOOT_ARMOR : {
            _gear.addFootArmor(gearitem);
            break;
        }
        case GearCode::ACCESSOIRE : {
            _gear.addAccessoire(gearitem);
            break;
        }
        default : { }
        }
    }
}

/**
 * @brief Player::getWeaponXp returns weapon xp
 * @return weapon xp
 */
num Player::getWeaponXp() const {
    return _weaponXp;
}

/**
 * @brief Player::getFireXp returns fire xp
 * @return fire xp
 */
num Player::getFireXp() const {
    return _fireXp;
}

/**
 * @brief Player::getWaterXp returns water xp
 * @return water xp
 */
num Player::getWaterXp() const {
    return _waterXp;
}

/**
 * @brief Player::getEarthXp returns earth xp
 * @return earth xp
 */
num Player::getEarthXp() const {
    return _earthXp;
}

/**
 * @brief Player::getWeaponLevel returns actual weapon level
 * @return weapon level
 */
num Player::getWeaponLevel() const {
    return _weaponLv;
}

/**
 * @brief Player::getFireLevel returns actual fire level
 * @return fire level
 */
num Player::getFireLevel() const {
    return _fireLv;
}

/**
 * @brief Player::getWindLevel returns actual wind level
 * @return wind level
 */
num Player::getWindLevel() const {
    return _windLv;
}

/**
 * @brief Player::getWaterLevel returns actual water level
 * @return water level
 */
num Player::getWaterLevel() const {
    return _waterLv;
}

/**
 * @brief Player::getEarthLevel returns actual earth level
 * @return earth level
 */
num Player::getEarthLevel() const {
    return _earthLv;
}

/**
 * @brief Player::getXp returns the actual xp from the player
 * @return xp value
 */
num Player::getXp() const {
    return _actXp;
}

/**
 * @brief Player::getXpToNextLevel returns the xp what is needed to level up
 * @return xp value to level up
 */
num Player::getXpToNextLevel() const {
    return _xpToLevelUp;
}

Group* Player::getPlayersGroup() {
    return _group;
}
