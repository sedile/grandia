#ifndef PLAYER_H
#define PLAYER_H

#include <string>
#include <iostream>

#include "Role.h"
#include "Gear.h"
#include "Group.h"

namespace grandia {

    class Player : public Role {
    private:
        num _actXp, _xpToLevelUp;
        num _weaponXp, _fireXp, _windXp, _waterXp, _earthXp;
        num _weaponLv, _fireLv, _windLv, _waterLv, _earthLv;
        Gear _gear;
        Group *_group;
    public:
        explicit Player(std::string name, num mhp, num mmp, num msp, num level, num actXP, num xpToLevelUp, num str, num def, num spd, num mov);
        virtual ~Player();

        void setPlayersGroup(Group *group);
        void setXP(num exp);
        void setXpToLevelUp(num xp);
        void equipGear(GearItem *gearitem, num piece, bool remove);

        void addWeaponXp(num &xp);
        void addFireXp(num &xp);
        void addWindXp(num &xp);
        void addWaterXp(num &xp);
        void addEarthXp(num &xp);

        void setWeaponLevel(num lv);
        void setFireLevel(num lv);
        void setWindLevel(num lv);
        void setWaterLevel(num lv);
        void setEarthLevel(num lv);

        num getWeaponXp() const;
        num getFireXp() const;
        num getWindXp() const;
        num getWaterXp() const;
        num getEarthXp() const;

        num getWeaponLevel() const;
        num getFireLevel() const;
        num getWindLevel() const;
        num getWaterLevel() const;
        num getEarthLevel() const;

        num getXp() const;
        num getXpToNextLevel() const;
        Group* getPlayersGroup();
    };

}

#endif // PLAYER_H
