#include <math.h>
#include "Point.h"

using namespace grandia;

/**
 * @brief Point::Point Pointclass
 * @param x position x on the battlefield
 * @param y position y on the battlefield
 */
Point::Point(num x, num y)
: _x(x), _y(y), RADIUS(50) { }

/**
 * @brief Point::~Point Destructor
 */
Point::~Point() { }

/**
 * @brief Point::gotoCoord set new coordinates
 * @param x new actor x position
 * @param y new actor y position
 */
void Point::gotoCoord(num &x, num &y){
    _x = x;
    _y = y;
}

/**
 * @brief Point::inRange checks if a target is in the range for Radius attacks
 * @param p point to be tested
 * @return true, in range, false otherwise
 */
bool Point::inRange(Point *p){
    if ( p != nullptr ) {
        double distance = sqrt(pow(p->getX() - _x,2) + pow(p->getY() - _y,2));
        if ( distance <= RADIUS ) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

/**
 * @brief Point::getX get position x
 * @return x
 */
num Point::getX() const {
    return _x;
}

/**
 * @brief Point::getY get position y
 * @return y
 */
num Point::getY() const {
    return _y;
}

/**
 * @brief Point::getRadius return radius
 * @return r
 */
num Point::getRadius() const {
    return RADIUS;
}
