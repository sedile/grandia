#ifndef __POINT_H__
#define __POINT_H__

namespace grandia {

    typedef unsigned short num;

    class Point {
    private:
        num _x, _y;
        const num RADIUS;
    public:
        explicit Point(num x, num y);
        virtual ~Point();

        void gotoCoord(num &x, num &y);

        bool inRange(Point *p);

        num getX() const;
        num getY() const;
        num getRadius() const;
    };

}

#endif
