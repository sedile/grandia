This file explains how to use the parameters to construct Players, Magic, Moves and Items

For Players/Enemies:

name = Name of the Player/Enemy
mhp = Maximum HP (Health Points) Value
mmp = Maximum MP (Mana Points) Value
msp = Maximum SP (Skill Points) Value
level = The level for the Player/Enemy
actXp = Player : The actual XP for this player / Enemy : If the Enemy is defeated the Player gets this amount of Xp
xpToLevelUp = How many XP is needed to Level up (Not supported yet)
money = Player : The amount of Groschen for the Player / Enemy : If the Enemy is defeated the Player gets this amount of Groschen
str = The strength value for this Player/Enemy (Damage)
def = The defense value for this Player/Enemy (Damage reduction)
spd = The speed value for this Player/Enemy (IP-Gauge speed)
mov = The movement value for this Player/Enemy (Movement on Battlefield)

Example for Player:
Actor *p = new Player("Justin", 500, 50, 40, 2, 0, 78, 0, 18, 10, 15);

or

Actor *p = new Enemy("Marna Bug",45,35,0,4,2,5,4,4,50,3);

A Boss (with or without Parts) can created too, but without enemies.

Example for Boss with Parts

Actor *boss = new Boss("Hydra (Main)",true,50,0,0,1,50,50,5,5,5,5);
Actor *bosspart1 = new Boss("Hydra - Heathead",false,700,5,5,5,5,5,5,5,5,5);
Actor *bosspart2 = new Boss("Hydra - Nicehead",false,700,5,5,5,5,5,5,5,5,5);
Actor *bosspart3 = new Boss("Hydra - Badhead",false,700,5,5,5,5,5,5,5,5,5);

The 'mainpart(s)' of the boss must be true (second parameter) means, if this part is defeated, all other bossparts are defeated too.

How to create Magic:

name = Name of this Spell
desc = Description of this Spell
mpu = Manacost for this Spell
type = healspell, attackspell, ...
distance = Single Target, Area Targets or All Targets
statusflag = which status ailment this spell can trigger (poison, stop, magic sap)
elem = base element of this spell (fire,wind,water,earth)
el = effectlevel (like Grandia Xtreme each Spell has an effectlevel)

For the parameters type, distance, statusflag and elem there exists special numbers

Supported Attacktypes:
AttackCode::ATTACK = A attack spell
AttackCode::HEAL = A heal spell
AttackCode::BOOST = Positive Fight Effect (ATK+,DEF+,SPD+ for a short time)
AttackCode::DECREASE = Negative Fight Effect against Enemies (ATK-,DEF-,SPD- for a short time)

Supported Special Types (Only for Magic)
SpecialCode::HALVAH
SpecialCode::RESURRECT
SpecialCode::VANISH

Supported Distances:
TargetCode::SINGLE_TARGET = Attacks one Enemy / Heals one Groupmember / Buffs one Groupmember / Debuffs one Enemy
TargetCode::RADIUS_TARGET = Attacks enemies in range (ex. Fireburner from Grandia or Tremor)
TargetCode::ALL_TARGET = Attacks all enemies (ex. Sky Dragon Slash) / Heals all Groupmembers

Supported Elements:
ElemCode::PHYSICAL = Physical attack
ElemCode::FIRE = Firebased attack
ElemCode::WIND = Windbased attack
ElemCode::WATER = Waterbased attack
ElemCode::EARTH = Earthbased attack

Supported Statusflags:
StatusCode::NO_STATUS = Normal attack with no status effect
StatusCode::ATK_UP = Increases the ATK Level for a short time
StatusCode::ATK_DOWN = Decreases the ATK Level for a short time
StatusCode::DEF_UP = Increases the DEF Level for a short time
StatusCode::DEF_DOWN = Decreases the DEF Level for a short time
StatusCode::SPD_UP = Increases the SPD Level for a short time
StatusCode::SPD_DOWN = Decreaes the SPD Level for a short time
StatusCode::MOV_UP = Increases the MOV Level
StatusCode::MOV_DOWN = Decreases the MOV Level
StatusCode::POISON = Poison Effect on Enemy/Player
StatusCode::POISON_HEAL = Heals Poison Effect (ex. give an item this Flag means this item can heal poison)
StatusCode::PARALYZE = Paralyzes on Enemy/Player
StatusCode::PARALYZE_HEAL = Heals the Paralyze effect
StatusCode::STOP = Stop effect on Enemy/Player
StatusCode::STOP_HEAL = Heals the stop effect
StatusCode::MAGIC_BLOACK = Player or Enemy can not use magic spells
StatusCode::MAGIC_BLOCK_HEAL = Undo the magic block
StatusCode::MOVE_BLOCK = Player or Enemy can not use move spells
StatusCode::MOVE_BLOCK_HEAL = Undo the move block
StatusCode::CURSE = Curse to one Player/Enemy
StatusCode::CURSE_HEAL = Heals the curse effect
StatusCode::ILLNES = Illnes effect to one player / enemy. Can cause all other negative status effects if not healed in short time
StatusCode::ILLNES_HEAL = Heals illnes (ex. Halvah)
StatusCode::GIGA_VANISH = Avoids all status effects. Not able to buff or debuff
StatusCode::GIGA_VANISH_HEAL = Heals giga vanish

Let us create the Fireburner spell from Grandia 1.
Magic *fireburner = new Magic("Fireburner","Burns one enemy with hell fire",35,AttackCode::ATTACK,TargetCode::SINGLE_TARGET,StatusCode::NO_STATUS,ElemCode::FIRE,9);

After you have created some Magic Spells you have to create one Mana Egg to store up to 10 Magic Spells

std::vector<Magic*> magicvector;
magicvector.pushback(fireburner);
...

ManaEgg *holyEgg = new ManaEgg(magicvector);

How to create Moves. It is nearly the same like magic spells.

First i will explain the parameters

name = Name of this Move
desc = Description of this Move
pu = skillpointcost for this Move
type = attack, heal (like magic)
distance = Single Target, Area Targets or All Targets
elem = base element of this move (physical,fire,wind,water,earth)
el = effectlevel (like Grandia Xtreme each Skill has a effectlevel)

All Codes from above are valid to create Moves (and Items)

We want to create the Spark Volt Move

Move *sparkvolt = new Move("Spark Volt","Attacks enemies in distance with volt power",24,AttackCode::ATTACK,TargetCode::RADIUS_TARGET,ElemCode::WIND,9);

You need a movecontainer to store your moves, like Mana Eggs.

std::vector<Move*> movevector;
movevector.push_back(sparkvolt);

MoveContainer *moves = new MoveContainer(movevector);

So, now we can create two types of items. StandardItems are all Items which you can use for fights and GearItems are items like swords, staffs, armor, boots (Not supported yet).

To create Items, it is the same like magic or moves.

We create the cure powder item. It heals all your allies with 2000 HP.

Item *curepowder = new StandardItem(true,"Cure Powder","Heals all members 2000 HP", 2000, ElemCode::WATER, TargetCode::ALL_TARGET, AttackCode::HEAL, StatusCode::NO_STATUS); 

Items are stored in a big bag. Let us create one

std::vector<Item*> itemvector;
itemvector.push_back(curepoweder);

Bag *bag = new Bag(itemvector);

We have created players and enemies, magic spells, moves and items. Now we want to fight

The Main.cpp contains a demo (WIP)

Compile with : g++ -std=c++17 -o grandia *.cpp
