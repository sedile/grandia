#include "Role.h"

using namespace grandia;

/**
 * @brief Role::Role  This is the baseclass for Player and Enemy
 * @param name  Name of this actor
 * @param isPlayer  true, if this is a player, false if this is a enemy
 * @param mhp   Maximum HP Value
 * @param mmp   Maximum MP Value
 * @param msp   Maximum SP Value
 * @param level The level of this Actor
 * @param str   Strength of this Actor (Damage)
 * @param def   Defense of this Actor (Damage reduction)
 * @param spd   Speed of this Actor (Fill speed of the IP-Gauge)
 * @param mov   Move of this Actor (Movement in Battlefield)
 */
Role::Role(std::string name, bool isPlayer, num mhp, num mmp, num msp, num level, num str, num def, num spd, num mov)
: NAME(name), MAX_HP(mhp), MAX_MP(mmp), MAX_SP(msp), AKT(str), DEF(def), ACT(spd), MOV(mov), IS_PLAYER(isPlayer), _level(level), _strength(str), _defense(def), _speed(spd), _move(mov), _fireres(0), _windres(0), _waterres(0), _earthres(0) {
    _actHP = MAX_HP;
    _actMP = MAX_MP;
    _actSP = MAX_SP;
    _ip = 0;
    _target = 0;
    _actiontype = ' ';
    _castphrase = false;
    _defenseMode = false;
    _actAction = "Wait";
    _status = new Status;
}

/**
 * @brief Role::~Role The Destructor
 */
Role::~Role() {
    delete _choosedMagicAttack;
    delete _choosedMoveAttack;
    delete _choosedItem;
    delete _position;
    delete _status;
    delete _spellset;
    delete _moveset;
}

/**
 * @brief Role::initResistence Initializes the element resistence
 * 0 = immun, 1 = normal, 2 = effective
 * @param fire  fire and explosion resistence
 * @param wind  wind and lightning resistence
 * @param water water and ice resistence
 * @param earth earth and forest resistence
 */
void Role::initResistence(num fire, num wind, num water, num earth){
    if ( fire >= 0 && fire <= 2 && wind >= 0 && wind <= 2 &&
         water >= 0 && water <= 2 && earth >= 0 && earth <= 2 ){
        _fireres = fire;
        _windres = wind;
        _waterres = water;
        _earthres = earth;
    }
}

/**
 * @brief Role::setFireResistence change the fireresistence
 * @param fire value between 0 and 2
 */
void Role::setFireResistence(num fire){
    if ( fire >= 0 && fire <= 2 ){
        _fireres = fire;
    }
}

/**
 * @brief Role::setWindResistence change windresistence
 * @param wind value between 0 and 2
 */
void Role::setWindResistence(num wind){
    if ( wind >= 0 && wind <= 2){
        _windres = wind;
    }
}

/**
 * @brief Role::setWaterResistence change waterresistence
 * @param water value between 0 and 2
 */
void Role::setWaterResistence(num water){
    if ( water >= 0 && water <= 2 ){
        _waterres = water;
    }
}

/**
 * @brief Actor::setEarthResistence change the earthresistence
 * @param earth value between 0 and 2
 */
void Role::setEarthResistence(num earth){
    if ( earth >= 0 && earth <= 2 ){
        _earthres = earth;
    }
}

/**
 * @brief Role::setHP change the HP value
 * @param value value can be positive (heal) or negative (damage)
 */
void Role::setHP(short value){
    if ( _actHP + value >= MAX_HP ) {
        _actHP = MAX_HP;
    } else {
        if ( _actHP + value <= 0 ){
            _actHP = 0;
        } else {
            _actHP += value;
        }
    }
}

/**
 * @brief Role::setMP cahge manapoints
 * @param value value can be positive or negative
 */
void Role::setMP(short value){
    if ( _actMP + value >= MAX_MP ) {
        _actMP = MAX_MP;
    } else {
        if ( _actMP + value <= 0 ){
            _actMP = 0;
        } else {
            _actMP += value;
        }
    }
}

/**
 * @brief Role::setSP change skillpoints for move attacks
 * @param value value can be positive or negative
 */
void Role::setSP(short value){
    if ( _actSP + value >= MAX_SP ) {
        _actSP = MAX_SP;
    } else {
        if ( _actSP + value <= 0 ){
            _actSP = 0;
        } else {
            _actSP += value;
        }
    }
}

/**
 * @brief Role::setLevel change the level of this actor
 * @param level level value
 */
void Role::setLevel(num level){
    _level = level;
}

/**
 * @brief Role::resetIP sets the IP value to 0
 */
void Role::resetIP(){
    _ip = 0;
}

/**
 * @brief Role::setIP changes the IP value
 * @param value new ip value
 */
void Role::setIP(short value){
    if ( _ip + value < 0 ){
        resetIP();
    } else {
        _ip += value;
    }
}

/**
 * @brief Role::setCastPhraseFlag actor prepares to cast a spell, uses item or uses a skill
 * @param cast true, if the actor prepares to cast (Between COM and ACT in the IP-Gauge)
 * false, if the actor awaits for the COM in the IP-Gauge
 */
void Role::setCastPhraseFlag(bool cast){
    _castphrase = cast;
}

/**
 * @brief Role::setStrength sets the strength value, example for ATK_UP or ATK_DOWN
 * @param value value for strength
 */
void Role::setStrength(short value){
    _strength = value;
}

/**
 * @brief Role::setDefense changes the defense value
 * @param value value for defense
 */
void Role::setDefense(short value){
    _defense = value;
}

/**
 * @brief Role::setSpeed changes the speed value
 * @param value value for speed
 */
void Role::setSpeed(short value){
    _speed = value;
}

/**
 * @brief Role::setMove changes the move value
 * @param value value for move
 */
void Role::setMove(short value){
    _move = value;
}

/**
 * @brief Role::setDefenseMode switches between defense mode and normal mode if the actor chooses defense int the wheelmenu
 * @param defmode true, if the actor defend himself, false otherwise
 */
void Role::setDefenseMode(bool defmode){
    _defenseMode = defmode;
}

/**
 * @brief Role::setPreparedAction Gives information about the actor what he does
 * @param action defined action value (ex. 'i' for info, 'c' for combo)
 */
void Role::setPreparedAction(const char &action){
    _actiontype = action;
}

/**
 * @brief Role::setTarget sets the actors target in the battlefield. Target can be a groupmember for healing or buffing.
 * @param target index of the actor in the battlefield
 */
void Role::setTarget(num target){
    _target = target;
}

/**
 * @brief Role::updateActorPosition set new position in battlefield, if actor evades or changes his position (NOT IMPLEMENTED YET)
 * @param x position x
 * @param y position y
 */
void Role::updateActorPosition(num &x, num &y){
    _position->gotoCoord(x,y);
}

/**
 * @brief Role::setSpellContainer give the actor a container which contains magicspells. It is a containerclass for spells
 * @param spellset set of magic spells
 */
void Role::setSpellContainer(SpellContainer *spellset){
    if ( spellset != nullptr ){
        _spellset = spellset;
    } else {
        _spellset = nullptr;
    }
}

/**
 * @brief Role::setMoveContainer give the actor a skillbook which contains movves. It is a containerclass for moves/skills
 * @param moveSet skillbook
 */
void Role::setMoveContainer(MoveContainer *moveSet){
    if ( moveSet != nullptr ){
        _moveset = moveSet;
    } else {
        _moveset = nullptr;
    }
}

/**
 * @brief Role::chooseMagicAttack Actor will cast this spell to a target
 * @param magic magic spell what will be used. Costs MP
 */
void Role::chooseMagicAttack(Magic *magic){
    if ( magic != nullptr ) {
        _choosedMagicAttack = magic;
    } else {
        _choosedMagicAttack = nullptr;
    }
}

/**
 * @brief Role::chooseMoveAttack Actor will cast this move to a target
 * @param move move/skill what will be used. costs SP
 */
void Role::chooseMoveAttack(Move *move){
    if ( move != nullptr ) {
        _choosedMoveAttack = move;
    } else {
        _choosedMoveAttack = nullptr;
    }
}

/**
 * @brief Role::chooseItem Player can choose an item meanwhile the fight. Player only
 * @param item This item will be used.
 */
void Role::chooseItem(Item *item){
    if ( item != nullptr ) {
        _choosedItem = item;
    } else {
        _choosedItem = nullptr;
    }
}

/**
 * @brief Role::setInitActorPosition Actors get a specific position on battlefield (NOT IMPLEMENTED YET)
 * @param x position x
 * @param y position y
 */
void Role::setInitActorPosition(num x, num y){
    _position = new Point(x,y);
}

/**
 * @brief Role::updateActorsAction Gives the string what is shown in the Statuspanel for this Actor
 * @param action command for the statuspanel
 */
void Role::updateActorsAction(std::string action){
    _actAction = action;
}

/**
 * @brief Role::getFireResistence Info about the fireresistence
 * @return fireresistence
 * 0 = immun
 * 1 = normal
 * 2 = effective
 */
num Role::getFireResistence() const {
    return _fireres;
}

/**
 * @brief Role::getWindResistence Info about the windresistence
 * @return  windresistence
 * 0 = immun
 * 1 = normal
 * 2 = effective
 */
num Role::getWindResistence() const {
    return _windres;
}

/**
 * @brief Role::getWaterResistence Info about the waterresistence
 * @return  waterresistence
 * 0 = immun
 * 1 = normal
 * 2 = effective
 */
num Role::getWaterResistence() const {
    return _waterres;
}

/**
 * @brief Role::getEarthResistence Info about the earthresistence
 * @return  earthresistence
 * 0 = immun
 * 1 = normal
 * 2 = effective
 */
num Role::getEarthResistence() const {
    return _earthres;
}

/**
 * @brief Role::getActHP returns the actual HP value of this actor
 * @return actual hp
 */
num Role::getActHP() const {
    return _actHP;
}

/**
 * @brief Role::getActMP returns the actual MP value of this actor
 * @return actual mp
 */
num Role::getActMP() const {
    return _actMP;
}

/**
 * @brief Role::getActSP returns the actual SP value of this actor
 * @return actual sp
 */
num Role::getActSP() const {
    return _actSP;
}

/**
 * @brief Role::getActHP returns the maximum HP value of this actor
 * @return maximum hp
 */
num Role::getMaxHP() const {
    return MAX_HP;
}

/**
 * @brief Role::getActMP returns the maximum MP value of this actor
 * @return maximum hp
 */
num Role::getMaxMP() const {
    return MAX_MP;
}

/**
 * @brief Role::getActSP returns the maximum SP value of this actor
 * @return maximum sp
 */
num Role::getMaxSP() const {
    return MAX_SP;
}

/**
 * @brief Role::getActHP returns the actual level of this actor
 * @return level
 */
num Role::getLevel() const {
    return _level;
}

/**
 * @brief Role::getIP get the actual IP value of this actor
 * @return ip value
 */
num Role::getIP() const {
    return _ip;
}

/**
 * @brief Role::getCastPhraseFlag returns the bit for castphrase
 * @return true, if actor is between COM and ACT in IP-Gauge, false otherwise
 */
bool Role::getCastPhraseFlag() const {
    return _castphrase;
}

/**
 * @brief Role::getStrength returns the strength value
 * @return strength value
 */
num Role::getStrength() const {
    return _strength;
}

/**
 * @brief Role::getDefense returns the defense value
 * @return defense value
 */
num Role::getDefense() const {
    return _defense;
}

/**
 * @brief Role::getSpeed returns the speed value
 * @return speed value
 */
num Role::getSpeed() const {
    return _speed;
}

/**
 * @brief Role::getMove returns the move value
 * @return move value
 */
num Role::getMove() const {
    return _move;
}

/**
 * @brief Role::getOriginStrength returns the original unmodifed strength value. This is needed beacause
 * the AKT_UP boost for example changes the strength value.
 * @return initial strength value
 */
num Role::getOriginStrength() const {
    return AKT;
}

/**
 * @brief Role::getOriginDefense returns the original unmodifed defense value. This is needed beacause
 * the DEF_UP boost for example changes the defense value.
 * @return initial defense value
 */
num Role::getOriginDefense() const {
    return DEF;
}

/**
 * @brief Role::getOriginSpeed returns the original unmodifed speed value. This is needed beacause
 * the SPD_UP boost for example changes the speed value.
 * @return inital speed value
 */
num Role::getOriginSpeed() const {
    return ACT;
}

/**
 * @brief Role::getOriginMove returns the original unmodifed move value. This is needed beacause
 * the MOV_UP boost for example changes the move value.
 * @return initial move value
 */
num Role::getOriginMove() const {
    return MOV;
}

/**
 * @brief Role::getDefenseModeFlag returns if the actor is in defense mode. Damage to actor will be halfed
 * @return true, if the actor is in defense mode, false otherwise
 */
bool Role::getDefenseModeFlag() const {
    return _defenseMode;
}

/**
 * @brief Role::getName returns the actors name
 * @return name as string
 */
std::string Role::getName() const {
    return NAME;
}

/**
 * @brief Role::isPlayer Actor is a human player
 * @return true, if the actor is a human, false if the actor is an enemy
 */
bool Role::isPlayer() const {
    return IS_PLAYER;
}

/**
 * @brief Role::getActionType returns the choosen action. Combo, Critical, ...
 * @return the action type
 */
char Role::getActionType() const {
    return _actiontype;
}

/**
 * @brief Role::getTarget returns the actors target
 * @return index of the target
 */
num Role::getTarget() const {
    return _target;
}

/**
 * @brief Role::getActorPosition returns the position in the battlefield
 * @return actors position
 */
Point* Role::getActorPosition() const {
    return _position;
}

/**
 * @brief Role::getSpellContainer returns the spellcontainer for magic spells
 * @return spellcontainer
 */
SpellContainer* Role::getSpellContainer() const {
    return _spellset;
}

/**
 * @brief Role::getMoveContainer returns the movecontainer for skills
 * @return skillcontainer
 */
MoveContainer* Role::getMoveContainer() const {
    return _moveset;
}

/**
 * @brief Role::getChoosedMagic returns the choosed spell
 * @return magic spell
 */
Magic* Role::getChoosedMagic() const {
    return _choosedMagicAttack;
}

/**
 * @brief Role::getChoosedItem returns the choosed item
 * @return item
 */
Item* Role::getChoosedItem() const {
    return _choosedItem;
}

/**
 * @brief Role::getChoosedMove returns the choosed skill
 * @return skill
 */
Move* Role::getChoosedMove() const {
    return _choosedMoveAttack;
}

/**
 * @brief Roler::getStatus returns the Status for this actor
 * @return statusreference to activate/deactive status ailments
 */
Status* Role::getStatus() const {
    return _status;
}

/**
 * @brief Role::getActorsAction returns what the actor does
 * @return action as string
 */
std::string Role::getActorsAction() const {
    return _actAction;
}
