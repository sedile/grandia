#ifndef __ROLE_H__
#define __ROLE_H__

#include <string>
#include <iostream>

#include "Point.h"
#include "Status.h"
#include "SpellContainer.h"
#include "MoveContainer.h"
#include "Bag.h"

namespace grandia {

    typedef unsigned short num;

    class Role {
    private:
        const std::string NAME;
        const num MAX_HP, MAX_MP, MAX_SP, AKT, DEF, ACT, MOV;
        const bool IS_PLAYER;
        num _actHP, _actMP, _actSP, _level;
        num _ip, _strength, _defense, _speed, _move;
        double _fireres, _windres, _waterres, _earthres;
        bool _defenseMode, _castphrase;
        char _actiontype;
        num _target;
        std::string _actAction;

        Point *_position = nullptr;
        SpellContainer *_spellset = nullptr;
        MoveContainer *_moveset = nullptr;
        Status *_status = nullptr;
        Magic* _choosedMagicAttack = nullptr;
        Move* _choosedMoveAttack = nullptr;
        Item* _choosedItem = nullptr;

    public:
        explicit Role(std::string name, bool isPlayer, num mhp, num mmp, num msp, num level, num str, num def, num spd, num mov);
        virtual ~Role();

        void initResistence(num fire, num wind, num water, num earth);
        void setFireResistence(num fire);
        void setWindResistence(num wind);
        void setWaterResistence(num water);
        void setEarthResistence(num earth);
        void setHP(short nhp);
        void setMP(short nmp);
        void setSP(short nsp);
        void setLevel(num level);
        void resetIP();
        void setIP(short ip_value);
        void setCastPhraseFlag(bool prepare);
        void setStrength(short str);
        void setDefense(short def);
        void setSpeed(short spd);
        void setMove(short mov);
        void setDefenseMode(bool def_mode);
        void setPreparedAction(const char &cmd);
        void setTarget(num target);
        void setSpellContainer(SpellContainer *spellset);
        void setMoveContainer(MoveContainer *monveSet);
        void chooseMagicAttack(Magic *magic);
        void chooseMoveAttack(Move *move);
        void chooseItem(Item *item);
        void updateActorsAction(std::string action);
        void setInitActorPosition(num x, num y);
        void updateActorPosition(num &x, num &y);

        num getFireResistence() const;
        num getWindResistence() const;
        num getWaterResistence() const;
        num getEarthResistence() const;
        num getActHP() const;
        num getActMP() const;
        num getActSP() const;
        num getMaxHP() const;
        num getMaxMP() const;
        num getMaxSP() const;
        num getLevel() const;
        num getIP() const;
        bool getCastPhraseFlag() const;
        num getStrength() const;
        num getDefense() const;
        num getSpeed() const;
        num getMove() const;
        num getOriginStrength() const;
        num getOriginDefense() const;
        num getOriginSpeed() const;
        num getOriginMove() const;
        bool getDefenseModeFlag() const;
        std::string getName() const;
        bool isPlayer() const;
        char getActionType() const;
        num getTarget() const;
        Point* getActorPosition() const;
        SpellContainer* getSpellContainer() const;
        MoveContainer* getMoveContainer() const;
        Magic* getChoosedMagic() const;
        Move* getChoosedMove() const;
        Item* getChoosedItem() const;
        Status* getStatus() const;
        std::string getActorsAction() const;
    };

}

#endif
