#include "SpellContainer.h"
#include <iostream>

using namespace grandia;

/**
 * @brief SpellContainer::SpellContainer Containerclass for Magicspells
 * @param magiclist vector of magicspells
 */
SpellContainer::SpellContainer(std::vector<Magic*> &magiclist){
    if ( !magiclist.empty() && magiclist.size() <= 10 ){
        _magiclist = magiclist;
    } else {
        std::cout << "Error at Spellcontainer Init : To much spells" << "\n";
    }
}

/**
 * @brief SpellContainer::~SpellContainer Destructor. Deletes all spells in the list
 */
SpellContainer::~SpellContainer() {
    for(num i = 0; i < _magiclist.size(); i++){
        delete _magiclist.at(i);
    }
}

/**
 * @brief SpellContainer::printMagicList prints informations about the spells in the list
 */
void SpellContainer::printMagicList() {
    for(num i = 0; i < _magiclist.size(); i++){
        std::cout << "[" << std::to_string(i) << "] ";
        std::cout << _magiclist.at(i)->getName() << " | ";
        std::cout << _magiclist.at(i)->getDescription() << " | ";
        std::cout << std::to_string(_magiclist.at(i)->getMPUse()) << " | ";
        std::cout << _magiclist.at(i)->getType() << " | ";
        std::cout << std::to_string(_magiclist.at(i)->getEffectLevel()) << "\n";
    }
}

/**
 * @brief SpellContainer::getMagic returns one spell from the list
 * @param idx index of the spell
 * @return spell
 */
Magic* SpellContainer::getMagic(num &idx) const {
    if ( idx > _magiclist.size() ){
        return nullptr;
    }
    return _magiclist.at(idx);
}

/**
 * @brief SpellContainer::getSize returns the containers size
 * @return size
 */
num SpellContainer::getSize() const {
    return _magiclist.size();
}
