#ifndef __SPELLCONTAINER_H__
#define __SPELLCONTAINER_H__

#include <string>
#include <vector>

#include "Magic.h"

namespace grandia {

    class SpellContainer {
    private:
        std::vector<Magic*> _magiclist;
    public:
        explicit SpellContainer(std::vector<Magic*> &magiclist);
        virtual ~SpellContainer();

        void printMagicList();
        Magic* getMagic(num &idx) const;
        num getSize() const;
    };

}

#endif
