#include "StandardItem.h"

using namespace grandia;

/**
 * @brief StandardItem::StandardItem Standarditem class
 * @param isStandardItem must be true
 * @param name Name of this standarditem
 * @param desc description of this standarditem
 * @param value value of this sandarditem
 * @param elem element of this standarditem (fire,wind,water,earth)
 * @param distance distance (single,radius,all)
 * @param type type of this item (attack,heal,buff,decrease)
 * @param flag what statusflag this item can cause
 * @param item_value value of this item for vendors
 */
StandardItem::StandardItem(bool isStandardItem, std::string name, std::string desc, num value, num elem, num distance, num type, num flag, num item_value)
    : Item(isStandardItem, name,desc,item_value), VALUE(value), ELEMENT(elem), DISTANCE(distance), TYPE(type), STATUS_FLAG(flag) { }

/**
 * @brief StandardItem::~StandardItem Destructor
 */
StandardItem::~StandardItem() { }

/**
 * @brief StandardItem::getValue Returns the Value of an Item. This describes how many
    damage this item will cause, or how much HP will be restored
 * @return the Item Value
 */
num StandardItem::getValue() const {
    return VALUE;
}

/**
 * @brief StandardItem::getElement Returns the Element of this item
 * @return the Elementtype
 */
num StandardItem::getElement() const {
    return ELEMENT;
}

/**
 * @brief StandardItem::getType Returns the Type of this Item
 * @return type (attack,heal,buff,decrease)
 */
num StandardItem::getType() const {
    return TYPE;
}

/**
 * @brief StandardItem::getDistance Returns the Item's distance
 * @return distance (single,radius,all)
 */
num StandardItem::getDistance() const {
    return DISTANCE;
}

/**
 * @brief StandardItem::getStatusFlag Returns the Statuscode which can be caused throgh this item
 * @return the statusflag
 */
num StandardItem::getStatusFlag() const {
    return STATUS_FLAG;
}
