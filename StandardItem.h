#ifndef STANDARDITEM_H
#define STANDARDITEM_H

#include <string>

#include "Item.h"

namespace grandia {

    class StandardItem : public Item {
    private:
        const num VALUE, ELEMENT, DISTANCE, TYPE, STATUS_FLAG;
    public:
        explicit StandardItem(bool isStandardItem, std::string name, std::string desc, num value, num elem, num distance, num type, num flag, num item_value);
        virtual ~StandardItem();

        num getValue() const;
        num getType() const;
        num getElement() const;
        num getDistance() const;
        num getStatusFlag() const;
        num getNumberOfHits() const;
    };

}

#endif // STANDARDITEM_H

