#include <iostream>
#include "Status.h"

using namespace grandia;

/**
 * @brief Status::Status A container for Status Ailments
 */
Status::Status()
: _attackboost(0), _defenseboost(0), _speedboost(0), _paralyzeTimer(5000), _stopTimer(4000), _gigavanishTimer(8000) { }

/**
 * @brief Status::~Status Destructor
 */
Status::~Status() { }

/**
 * @brief Status::setAttackBoost Changes the Attacklevel
 * @param mod value of the new attacklevel
 */
void Status::setAttackBoost(short mod){
    if ( mod > 0 ){
        if ( _attackboost + mod > 5){
            _attackboost = 5;
        } else {
            _attackboost += mod;
        }
    } else {
        if ( _attackboost + mod < -5 ){
            _attackboost = -5;
        } else {
            _attackboost += mod;
        }
    }
}

/**
 * @brief Status::setDefenseBoost Changes the Defenselevel
 * @param mod value of the new defenselevel
 */
void Status::setDefenseBoost(short mod){
    if ( mod > 0 ){
        if ( _defenseboost + mod > 5){
            _defenseboost = 5;
        } else {
            _defenseboost += mod;
        }
    } else {
        if ( _defenseboost + mod < -5 ){
            _defenseboost = -5;
        } else {
            _defenseboost += mod;
        }
    }
}

/**
 * @brief Status::setSpeedBoost Changes the Speedlevel
 * @param mod value of the new speedlevel
 */
void Status::setSpeedBoost(short mod){
    if ( mod > 0 ){
        if ( _speedboost + mod > 5){
            _speedboost = 5;
        } else {
            _speedboost += mod;
        }
    } else {
        if ( _speedboost + mod < -5 ){
            _speedboost = -5;
        } else {
            _speedboost += mod;
        }
    }
}

/**
 * @brief Status::setMoveBoost Changes the Movementlevel
 * @param mod value of the new movementlevel
 */
void Status::setMoveBoost(short mod){
    if ( mod > 0 ){
        if ( _moveboost + mod > 5){
            _moveboost = 5;
        } else {
            _moveboost += mod;
        }
    } else {
        if ( _moveboost + mod < -5 ){
            _moveboost = -5;
        } else {
            _moveboost += mod;
        }
    }
}

/**
 * @brief Status::resetBoost reset all boost parameter to zero. Used by the Vanish effect
 */
void Status::resetBoost(){
    _attackboost = 0;
    _defenseboost = 0;
    _speedboost = 0;
    _moveboost = 0;
}

/**
 * @brief Status::resetAilments reset all status ailments. Used by Halvah
 */
void Status::resetAilments(){
    setPoisonFlag(false);
    setParalyzeFlag(false);
    setStopFlag(false);
    setMagicBlockFlag(false);
    setMoveBlockFlag(false);
    setCurseFlag(false);
    setIllnesFlag(false);
    setGigaVanishFlag(false);

    _stopTimer = 0;
    _paralyzeTimer = 0;
    _gigavanishTimer = 0;
}

/**
 * @brief Status::setPoisonFlag Changes the Poisonflag
 * @param poison true, actor is poisoned
 */
void Status::setPoisonFlag(bool poison){
    _poison = poison;
}

/**
 * @brief Status::setParalyzeFlag Changes the paralyzeflag
 * @param paralyze true, actor is paralyzed
 */
void Status::setParalyzeFlag(bool paralyze){
    _paralyze = paralyze;
}

/**
 * @brief Status::setStopFlag Changes the stopflag
 * @param stop true, actor can not do anything
 */
void Status::setStopFlag(bool stop){
    _stop = stop;
}

/**
 * @brief Status::setMagicBlockFlag Changes the magicblockflag
 * @param magicblock true, actor can not use magic
 */
void Status::setMagicBlockFlag(bool magicblock){
    _magicblock = magicblock;
}

/**
 * @brief Status::setMoveBlockFlag Changes the moveblockflag
 * @param moveblock true, actor can not use moves
 */
void Status::setMoveBlockFlag(bool moveblock){
    _moveblock = moveblock;
}

/**
 * @brief Status::setCurseFlag Changes the curseflag
 * @param curse true, if the actor is cursed
 */
void Status::setCurseFlag(bool curse){
    _curse = curse;
}

/**
 * @brief Status::setIllnesFlag Changes the illnesflag
 * @param illnes true, if the actor is ill
 */
void Status::setIllnesFlag(bool illnes){
    _illnes = illnes;
}

/**
 * @brief Status::setGigaVanishFlag Changes the gigavanishflag
 * @param gigavanish true, if the actor can not buff
 */
void Status::setGigaVanishFlag(bool gigavanish){
    _gigavanish = gigavanish;
}

/**
 * @brief Status::gigavanish Activate Giga Vanish
 */
void Status::gigavanish(){
    _attackboost = 0;
    _defenseboost = 0;
    _speedboost = 0;
}

/**
 * @brief Status::getAttackBoost returns the attacklevel
 * @return attacklevel
 */
short Status::getAttackBoost() const {
    return _attackboost;
}

/**
 * @brief Status::getDefenseBoost returns the defenselevel
 * @return defenselevel
 */
short Status::getDefenseBoost() const {
    return _defenseboost;
}

/**
 * @brief Status::getSpeedBoost returns the speedlevel
 * @return speedlevel
 */
short Status::getSpeedBoost() const {
    return _speedboost;
}

/**
 * @brief Status::getMoveBoost returns the movementlevel
 * @return movementlevel
 */
short Status::getMoveBoost() const {
    return _moveboost;
}

/**
 * @brief Status::getPoisonFlag returns the poisonflag
 * @return poisonflag
 */
bool Status::getPoisonFlag() const {
    return _poison;
}

/**
 * @brief Status::getParalyzeFlag returns the paralyzeflag
 * @return paralyzeflag
 */
bool Status::getParalyzeFlag() const {
    return _paralyze;
}

/**
 * @brief Status::getStopFlag returns the stopflag
 * @return stopflag
 */
bool Status::getStopFlag() const {
    return _stop;
}

/**
 * @brief Status::getMagicBlockFlag returns the magicblockflag
 * @return magicblockflag
 */
bool Status::getMagicBlockFlag() const {
    return _magicblock;
}

/**
 * @brief Status::getMoveBlockFlag returns the moveblockflag
 * @return moveblockflag
 */
bool Status::getMoveBlockFlag() const {
    return _moveblock;
}

/**
 * @brief Status::getCurseFlag returns the curseflag
 * @return curseflag
 */
bool Status::getCurseFlag() const {
    return _curse;
}

/**
 * @brief Status::getIllnesFlag returns the illnesflag
 * @return illnesflag
 */
bool Status::getIllnesFlag() const {
    return _illnes;
}

/**
 * @brief Status::getGigaVanishFlag returns the gigavanishflag
 * @return gigavanishflag
 */
bool Status::getGigaVanishFlag() const {
    return _gigavanish;
}

/**
 * @brief Status::showStatus extends the Statuspanel if the actor is affected by a status ailment
 */
void Status::showStatus() {
    std::cout << "AKT : " << std::to_string(getAttackBoost()) << " ";
    std::cout << "DEF : " << std::to_string(getDefenseBoost()) << " ";
    std::cout << "ACT : " << std::to_string(getSpeedBoost()) << " ";
    std::cout << "MOV : " << std::to_string(getMoveBoost()) << "\n";

    if( getPoisonFlag() ) {
        std::cout << "Poison" << "\n";
    }
    if( getParalyzeFlag() ) {
        std::cout << "Paralyze" << "\n";
    }
    if( getStopFlag() ) {
        std::cout << "Stop" << "\n";
    }
    if( getCurseFlag() ) {
        std::cout << "Curse" << "\n";
    }
    if( getIllnesFlag() ) {
        std::cout << "Illnes" << "\n";
    }
    if( getMagicBlockFlag() ) {
        std::cout << "Magic Block" << "\n";
    }
    if( getMoveBlockFlag() ) {
        std::cout << "Move Block" << "\n";
    }
    if( getGigaVanishFlag() ) {
        std::cout << "Giga Vanish" << "\n";
    }
    std::cout << "--------------------------------" << "\n";
}

/**
 * @brief Status::getParalyzeTimer returns the amount of time, how long the paralyze effects on the actor
 * @return paralyzetime
 */
num Status::getParalyzeTimer() const {
    return _paralyzeTimer;
}

/**
 * @brief Status::getStopTimer returns the amount of time, how long the stop effects on the actor
 * @return stoptime
 */
num Status::getStopTimer() const {
    return _stopTimer;
}

/**
 * @brief Status::getGigaVanishTimer returns the amount of time, how long the gigavanish effects on the actor
 * @return gigavanishtime
 */
num Status::getGigaVanishTimer() const {
    return _gigavanishTimer;
}

/**
 * @brief Status::resetParalyzeTimer resets the paralyze timer
 */
void Status::resetParalyzeTimer(){
    _paralyzeTimer = 5000;
}

/**
 * @brief Status::resetStopTimer resets the stop timer
 */
void Status::resetStopTimer(){
    _stopTimer = 5000;
}

/**
 * @brief Status::resetGigaVanishTimer resets the gigavanish timer
 */
void Status::resetGigaVanishTimer(){
    _gigavanishTimer = 5000;
}

/**
 * @brief Status::updateParalyzeTimer updates the paralyzetimer
 * @param ip ip value
 */
void Status::updateParalyzeTimer(num ip){
    if ( _paralyzeTimer - ip <= 0 ){
        _paralyzeTimer = 0;
    } else {
        _paralyzeTimer -= ip;
    }
}

/**
 * @brief Status::updateStopTimer updates the stoptimer
 * @param ip ip value
 */
void Status::updateStopTimer(num ip){
    if ( _stopTimer - ip <= 0 ){
        _stopTimer = 0;
    } else {
        _stopTimer -= ip;
    }
}

/**
 * @brief Status::updateGigaVanishTimer updates the gigavanishtimer
 * @param ip ip value
 */
void Status::updateGigaVanishTimer(num ip){
    if ( _gigavanishTimer - ip <= 0 ){
        _gigavanishTimer = 0;
    } else {
        _gigavanishTimer -= ip;
    }
}
