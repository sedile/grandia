#ifndef __STATUS_H__
#define __STATUS_H__

namespace grandia {

    typedef unsigned short num;

    class Status {
    private:
        short _attackboost, _defenseboost, _speedboost, _moveboost;
        bool _poison, _paralyze, _stop, _magicblock, _moveblock, _curse, _illnes, _gigavanish;
        num _paralyzeTimer, _stopTimer, _gigavanishTimer;
    public:
        explicit Status();
        virtual ~Status();

        void setAttackBoost(short mod);
        void setDefenseBoost(short mod);
        void setSpeedBoost(short mod);
        void setMoveBoost(short mod);
        void resetBoost();
        void resetAilments();
        void setPoisonFlag(bool poison);
        void setParalyzeFlag(bool paralyze);
        void setStopFlag(bool stop);
        void setMagicBlockFlag(bool magicblock);
        void setMoveBlockFlag(bool moveblock);
        void setCurseFlag(bool curse);
        void setIllnesFlag(bool illnes);
        void setGigaVanishFlag(bool gigavanish);
        void gigavanish();

        short getAttackBoost() const;
        short getDefenseBoost() const;
        short getSpeedBoost() const;
        short getMoveBoost() const;
        bool getPoisonFlag() const;
        bool getParalyzeFlag() const;
        bool getStopFlag() const;
        bool getMagicBlockFlag() const;
        bool getMoveBlockFlag() const;
        bool getCurseFlag() const;
        bool getIllnesFlag() const;
        bool getGigaVanishFlag() const;

        num getParalyzeTimer() const;
        num getStopTimer() const;
        num getGigaVanishTimer() const;

        void updateParalyzeTimer(num ip);
        void updateStopTimer(num ip);
        void updateGigaVanishTimer(num ip);

        void resetParalyzeTimer();
        void resetStopTimer();
        void resetGigaVanishTimer();

        void showStatus();
    };

}

#endif
