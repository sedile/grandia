#include "Vendor.h"

using namespace grandia;

/**
 * @brief Vendor::Vendor A vendor who can buy and sell items to the player outside a fight
 * @param name name of the vendor
 */
Vendor::Vendor(std::string name)
    : NAME(name) { }

/**
 * @brief Vendor::Vendor A vendor who can buy and sell items to the player outside a fight
 * @param name name of the vendor
 * @param inventar vendors inventar of items
 */
Vendor::Vendor(std::string name, const std::vector<Item*> &inventar)
    : NAME(name), _inventar(inventar) { }

/**
 * @brief Vendor::~Vendor Destructor
 */
Vendor::~Vendor(){
    for(unsigned short i = 0; i < _inventar.size(); i++){
        delete _inventar.at(i);
    }
}

/**
 * @brief Vendor::sellItem a function to sell items to the vendor
 * @param group group reference because of the itembag
 * @return amount of money
 */
num Vendor::sellItem(Group *group){
    if ( group == nullptr ){
        return 0;
    }

    group->getItemBag()->printItemList();
    short input;
    std::cout << "Sell what item? : " << "\n";
    std::cin >> input;

    if ( input < 0 || input > group->getItemBag()->getSize() ){
        std::cout << "Quit transaction" << "\n";
        return 0;
    }

    /* Our item what we want to sell to the vendor */
    Item *temp = group->getItemBag()->getItem(input);
    _inventar.push_back(new Item(temp->isStandardItem(), temp->getName(),temp->getDescription(),temp->getItemValue()));
    num value = static_cast<unsigned short>(temp->getItemValue() * 0.5f);
    std::cout << "Vendor buys " << temp->getName() << " for " << value << " Money\n";
    group->setMoney(value);
    group->getItemBag()->deleteItem();
    return value;
}

/**
 * @brief Vendor::buyItem a function to buy items from the vendor
 * @param group group reference because of the itembag
 */
void Vendor::buyItem(Group *group){
    printItemList();
    short input;
    std::cout << "Buy what item? : " << "\n";
    std::cin >> input;

    if ( input < 0 || static_cast<num>(input) > _inventar.size() ){
        std::cout << "Quit transaction" << "\n";
        return;
    }

    Item *temp = _inventar.at(input);
    if ( group->getMoney() < temp->getItemValue() ){
        std::cout << "You have not enough money\n";
    } else {
        group->setMoney(-temp->getItemValue());
        std::cout << "You bought " << temp->getName() << " for " << temp->getItemValue() << "\n";
        group->getItemBag()->addItem(temp);
    }
}

/**
 * @brief Vendor::printItemList prints the list of items from the vendor
 */
void Vendor::printItemList(){
    for(num i = 0; i < _inventar.size(); i++){
        std::cout << "(" << i << ") " << _inventar.at(i)->getName() << " [" << _inventar.at(i)->getItemValue() << "]\n";
    }
}
