#ifndef VENDOR_H
#define VENDOR_H

#include <vector>

#include "Item.h"
#include "Group.h"

namespace grandia {

class Vendor {
private:
    const std::string NAME;
    std::vector<Item*> _inventar;
public:
    explicit Vendor(std::string name);
    explicit Vendor(std::string name, const std::vector<Item*> &inventar);
    virtual ~Vendor();
    num sellItem(Group *group);
    void buyItem(Group *group);
    void printItemList();
};
}

#endif // VENDOR_H
